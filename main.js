/** module("g2w.entities").constant("ENTITIES") */
!function (window, angular, lodash) {
    "use strict";
    var entities = {
        "web.user.profile": {
            type: "user.profile",
            name: "web.user.profile",
            tags: ["web", "user", "profile"]
        },
        "web.user.watchlist.uplay": {
            type: "user.watchlist.uplay",
            name: "web.user.watchlist.uplay",
            tags: ["web", "user", "profile"]
        },
        "web.user.watchlist.psn": {
            type: "user.watchlist.psn",
            name: "web.user.watchlist.psn",
            tags: ["web", "user", "profile"]
        },
        "web.user.watchlist.xbl": {
            type: "user.watchlist.xbl",
            name: "web.user.watchlist.xbl",
            tags: ["web", "user", "profile"]
        }
    };
    angular.module("g2w.entities").constant("ENTITIES", entities)
}(window, window.angular);

/** module("g2w.entities").service("entitiesService") */
!function (window, angular, lodash, undefined) {
    "use strict";
    function entitiesService($log, $q, CONFIG, ENTITIES, entitiesApiService) {
        var entities = [];
        var currentId = "";
        function find(name) {
            return lodash.find(entities, {
                name: name
            })
        }
        function initialApiCall() {
            return entitiesApiService.getProfileEntitiesSearch({
                profileId: currentId,
                spaceId: CONFIG.spaceIds.g2w
            }).then(function (res) {
                return entities.splice(0, entities.length),
                    lodash.forEach(res.entities, function (entity) {
                        entities.push(entity)
                    }),
                    angular.copy(entities)
            })
        }
        function update(name, values) {
            var entity = find(name);
            if (angular.isUndefined(entity)) {
                return create(name, values);
            } else {
                entity.obj = values;
                $log.info("[entitiesService.service]", "update", values);

                return entitiesApiService.updateProfilesEntity(entity)
                    .then(function (entity) {
                        $log.info("[entitiesService.service]", "updated", entity);
                        return angular.copy(entity)
                    }, function (entity) {
                        $log.warn("[entitiesService.service]", "_update", entity)
                        return !1;
                    }).then(initialApiCall)

            }
        }
        function create(name, values) {
            $log.info("[entitiesService.service]", "create", currentId, values);
            var data = ENTITIES[name];
            var entity = {
                profileId: currentId,
                spaceId: CONFIG.spaceIds.g2w,
                type: data.type,
                name: data.name,
                tags: data.tags,
                obj: values
            };
            return entitiesApiService.createProfilesEntity(entity)
                .then(function (res) {
                    $log.info("[entitiesService.service]", "created", currentId, res);
                    return res;
                })
                .then(initialApiCall)
        }
        return {
            init: function (initialId) {
                currentId = initialId;
                return initialApiCall(currentId);
            },
            reset: function () {
                entities.splice(0, entities.length);
                currentId = "";
            },
            clear: function (name) {
                var entity = find(name);
                if (angular.isDefined(entity)) {
                    return entitiesApiService.deleteProfilesEntity({
                            entityId: entity.entityId
                        })
                        .then(function () {
                            return !0
                        })
                        .then(initialApiCall)
                } else {
                    return initialApiCall(currentId)
                }
            },
            getProperty: function (name, property) {
                var data;
                var entity = find(name);
                if (angular.isDefined(entity) && angular.isDefined(entity.obj)) {
                    data = angular.isUndefined(property)
                        ? entity.obj
                        : entitiy.obj[property]
                }
                return data;
            },
            updateProperty: function (name, propertyId, propertyValue) {
                if (angular.isUndefined(propertyId) || angular.isUndefined(propertyValue))
                    throw "entities.service.updateProperty needs propertyId and propertyValue to be defined";
                var entity = find(name);
                var newData = {};
                if (angular.isDefined(entity) && (newData = entity.obj)) {
                    newData[propertyId] = propertyValue;
                }
                return update(name, newData);
            },
            deleteProperty: function (name, propertyId) {
                var entity = find(name);
                if (angular.isDefined(entity)) {
                    var newValue = entity.obj;
                    return delete newValue[propertyId],
                        update(name, newValue)
                }
                return $q.when({})
            }
        }
    }
    entitiesService.$inject = ["$log", "$q", "CONFIG", "ENTITIES", "entitiesApiService"],
        angular.module("g2w.entities").service("entitiesService", entitiesService)
}(window, window.angular, window._);

/** odule("g2w.notifications").service("notificationsService") */
!function (window, angular, undefined) {
    "use strict";
    function notificationsService($timeout, NOTIFICATIONS) {
        function d(a) {
            f(a);
            var b = g.indexOf(a);
            g.splice(b, 1)
        }
        function e(b, c) {
            b.ttl = c,
                h.push(b),
                b.ttlTimeout = $timeout(function () {
                    d(b)
                }, c)
        }
        function f(b) {
            var c = h.indexOf(b);
            c >= 0 && ($timeout.cancel(b.ttlTimeout),
                h.splice(c, 1))
        }
        var g = [];
        var h = [];
        return {
            reset: function () {
                _.forEach(g, function (a) {
                    f(a)
                }),
                    g.splice(0, g.length)
            },
            add: function (a, f, h, i) {
                angular.isUndefined(h) && (h = NOTIFICATIONS.ttl);
                var j = {
                    tpl: a,
                    image: f.image,
                    data: f,
                    action: function (a) {
                        angular.isDefined(i) && i(a),
                            "close" === a && d(j)
                    }
                };
                h > 0 && e(j, h),
                    g.push(j)
            },
            list: g
        }
    }
    notificationsService.$inject = ["$timeout", "NOTIFICATIONS"],
        angular.module("g2w.notifications").service("notificationsService", notificationsService)
}(window, window.angular);

/** module("g2w.parser").service("profileParserService") */
!function (a, b, c, d) {
    "use strict";
    function e(a, d) {
        function e(a) {
            var b = {
                platformType: a.platformType,
                profileId: a.profileId,
                username: a.nameOnPlatform,
                userId: a.userId,
                avatar: {
                    smallUrl: d.uplayConnect.avatarBaseUrl + "/" + a.userId + "/default_146_146.png?appId=" + d.appIds.g2w,
                    mediumUrl: d.uplayConnect.avatarBaseUrl + "/" + a.userId + "/default_256_256.png?appId=" + d.appIds.g2w
                }
            };
            return b
        }
        function f(b) {
            var e = {}
                , f = c.keyBy(b.profiles, "platformType");
            return c.forEach(f, function (a, b) {
                d.platforms.indexOf(b) >= 0 && (e[b] = h.getProfile(a),
                    e[b].platformType = b)
            }),
                a.info("[parser.profile.service]", "search", "data", b, "result", e),
                e
        }
        function g(a) {
            var e, f = {};
            return c.forEach(a.profiles, function (a) {
                b.isUndefined(f[a.userId]) && (f[a.userId] = {}),
                    b.isUndefined(e) && (e = f[a.userId]),
                    d.platforms.indexOf(a.platformType) >= 0 && (f[a.userId][a.platformType] = h.getProfile(a))
            }),
                c.toArray(f)
        }
        var h = this;
        return b.extend(h, {
            getProfile: e,
            byPlatform: f,
            search: g
        })
    }
    e.$inject = ["$log", "CONFIG"],
        b.module("g2w.parser").service("profileParserService", e)
}(window, window.angular, window._);

/** module("g2w.parser").service("uplaywinParserService") */
!function (a, b, c, d) {
    "use strict";
    function e(a, b) {
        return {
            action: function (a) {
                var d = {};
                return d.id = a.id,
                    d.name = a.name,
                    d.isBadge = a.isBadge,
                    d.isCompleted = a.isCompleted,
                    d.isAvailable = a.available,
                    d.value = a.value,
                    d.image = c.find(a.images, {
                        type: "thumbnail"
                    }),
                    d.image = d.image ? b.uplayConnect.staticUrl + d.image.url : "",
                    d
            },
            actionList: function (a) {
                var b = this
                    , d = [];
                return c.forEach(a, function (a) {
                    d.push(b.action(a))
                }),
                    d
            }
        }
    }
    e.$inject = ["$log", "CONFIG"],
        b.module("g2w.parser").service("uplaywinParserService", e)
}(window, window.angular, window._);

/** module("g2w.search").service("searchService") */
!function (a, b, c, d) {
    "use strict";
    function e(a, b, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v) {
        function w(a) {
            return k.parsePlatform().then(function (b) {
                return a.request.platformType = b.type,
                    a
            })
        }
        function x(b) {
            function d(a) {
                return i.search({
                    nameOnPlatform: b.request.nameOnPlatform,
                    platformType: a
                })
            }
            f.debug("[index.service]", "search", "profiles", b);
            var e = {};
            return b.request.multiplatform ? c.forEach(g.platforms, function (a) {
                e[a] = d(a)
            }) : e[b.request.platformType] = d(b.request.platformType),
                a.all(e).then(function (a) {
                    var d = [];
                    c.forEach(a, function (a) {
                        c.forEach(a.profiles, function (a) {
                            d.push(l.getProfile(a))
                        })
                    });
                    c.groupBy(d, "userId");
                    return b.results.profiles = d,
                        b
                }, function () {
                    return f.warn("[index.service]", "multisearch error"),
                        b
                })
        }
        function y(a) {
            return f.debug("[index.service]", "search", "adddata", a),
                c.forEach(a.results.profiles, function (a) {
                    a.image = a.avatar.smallUrl
                }),
                a
        }
        function z(b) {
            if ("" === b)
                return a.when([]);
            var c = {
                request: {
                    nameOnPlatform: b,
                    multiplatform: !1
                },
                results: {}
            };
            return w(c).then(x).then(y).then(function (a) {
                return a.results.profiles
            })
        }
        return {
            search: z
        }
    }
    e.$inject = ["$q", "$rootScope", "$state", "$translate", "$log", "CONFIG", "INDEX", "profileApiService", "sessionService", "platformsService", "profileParserService", "r6KarmaApiService", "r6PlayerProfileApiService", "friendsApiService", "rsMoleculeModalService", "uplayConnectService", "notificationsService", "watchlistService", "entitiesService", "rsLayoutOffcanvasService", "statisticsService"],
        b.module("g2w.search").service("searchService", e)
}(window, window.angular, _);

/** module("g2w.statistics").service("statisticsCompareService") */
!function (a, b, c, d) {
    "use strict";
    function e(a, e, f, g, h, i, j) {
        function k() {
            l = [{
                id: "core-stats",
                label: "202303",
                statistics: [{
                    id: "clearance",
                    label: "190606"
                }, {
                    id: "rank",
                    label: "190605"
                }, {
                    label: "195515",
                    statId: "generalpvp_timeplayed"
                }, {
                    label: "195516",
                    statId: "generalpve_timeplayed"
                }]
            }, {
                id: "multiplayer-casual",
                label: "202304",
                statistics: [{
                    statId: "casualpvp_matchwlratio",
                    label: 192633
                }, {
                    statId: "casualpvp_kdratio"
                }, {
                    statId: "casualpvp_matchplayed"
                }]
            }, {
                id: "multiplayer-ranked",
                label: "202305",
                statistics: [{
                    statId: "rankedpvp_matchwlratio",
                    label: 192633
                }, {
                    statId: "rankedpvp_kdratio"
                }, {
                    statId: "rankedpvp_matchplayed"
                }]
            }, {
                id: "multiplayer-global",
                label: "202306",
                statistics: [{
                    statId: "generalpvp_kills"
                }, {
                    statId: "generalpvp_death"
                }, {
                    statId: "generalpvp_kdratio"
                }, {
                    statId: "rescuehostagepvp_bestscore"
                }, {
                    statId: "plantbombpvp_bestscore"
                }, {
                    statId: "secureareapvp_bestscore"
                }, {
                    statId: "generalpvp_revive"
                }, {
                    statId: "generalpvp_headshot"
                }, {
                    statId: "generalpvp_penetrationkills"
                }, {
                    statId: "generalpvp_meleekills"
                }, {
                    statId: "generalpvp_killassists"
                }]
            }, {
                id: "terrorist-hunt-best-score",
                label: "202307",
                statistics: [{
                    statId: "terrohuntclassicpve_bestscore"
                }, {
                    statId: "rescuehostagepve_bestscore"
                }, {
                    statId: "protecthostagepve_bestscore"
                }, {
                    statId: "allterrohuntsolo_normal_bestscore"
                }, {
                    statId: "allterrohuntcoop_normal_bestscore"
                }, {
                    statId: "allterrohuntsolo_hard_bestscore"
                }, {
                    statId: "allterrohuntcoop_hard_bestscore"
                }, {
                    statId: "allterrohuntsolo_realistic_bestscore"
                }, {
                    statId: "allterrohuntcoop_realistic_bestscore"
                }]
            }, {
                id: "terrorist-hunt-missions",
                label: "202308",
                statistics: [{
                    statId: "generalpve_matchplayed"
                }, {
                    statId: "plantbombpve_matchplayed"
                }, {
                    statId: "rescuehostagepve_matchplayed"
                }, {
                    statId: "protecthostagepve_matchplayed"
                }, {
                    statId: "terrohuntclassicpve_matchplayed"
                }, {
                    statId: "generalpve_matchwlratio",
                    label: 192633
                }, {
                    statId: "generalpve_matchwon"
                }, {
                    statId: "generalpve_matchlost"
                }]
            }, {
                id: "terrorist-hunt-global",
                label: "202309",
                statistics: [{
                    statId: "generalpve_matchwlratio",
                    label: 192633
                }, {
                    statId: "generalpve_matchplayed"
                }, {
                    statId: "generalpve_matchwon"
                }, {
                    statId: "generalpve_matchlost"
                }, {
                    statId: "generalpve_kdratio"
                }, {
                    statId: "generalpve_kills"
                }, {
                    statId: "generalpve_death"
                }, {
                    statId: "generalpve_killassists"
                }, {
                    statId: "generalpve_revive"
                }, {
                    statId: "generalpve_headshot"
                }, {
                    statId: "generalpve_penetrationkills"
                }, {
                    statId: "generalpve_meleekills"
                }]
            }],
                m = h.extractStatisticsIds(l),
                n = {},
                c.forEach(l, function (a) {
                    c.forEach(a.statistics, function (a) {
                        b.isDefined(a.statId) && (n[a.statId] = {
                            statId: a.statId,
                            label: a.label
                        })
                    })
                })
        }
        var l, m, n;
        return k(),
            {
                show: function (a) {
                    function e(d) {
                        return h.getUsersStatistics(a).then(function (a) {
                            d.categories[0].statistics[0].values = [],
                                d.categories[0].statistics[1].values = [];
                            return c.forEach(a, function (a) {
                                var c = 0;
                                d.categories[0].statistics[0].values.push(a.clearance),
                                    b.isDefined(a.ranks.current) && (c = a.ranks.current.icon),
                                    d.categories[0].statistics[1].values.push(c)
                            }),
                                d
                        }, function (a) {
                            return d
                        })
                    }
                    function f(e) {
                        return i.getStatistics(o).then(function (f) {
                            var g = {};
                            c.forEach(f.results, function (a, b) {
                                var d = c.clone(n);
                                g[b] = h.merge(a, d)
                            });
                            var i = c.cloneDeep(l);
                            return c.forEach(i, function (e) {
                                c.forEach(e.statistics, function (e) {
                                    b.isDefined(e.statId) && (b.isUndefined(e.values) && (e.values = []),
                                        c.forEach(a, function (a) {
                                            if (b.isUndefined(a.profileId))
                                                e.values.push(d);
                                            else {
                                                var c;
                                                g[a.profileId] && g[a.profileId][e.statId] && (c = g[a.profileId][e.statId]),
                                                    b.isDefined(c) && (e.type = c.type,
                                                        e.label = c.label),
                                                    b.isDefined(c) && b.isDefined(c.value) ? e.values.push(c.value) : e.values.push(d)
                                            }
                                        }))
                                })
                            }),
                                e.categories = i,
                                e
                        })
                    }
                    function g(a) {
                        j.openModal({
                            rsMoleculeModal: "assets/tpl/components/quick.compare.modal.tpl.html",
                            rsPropCloseOnBackdropClick: !0,
                            rsPropModalTitle: "193853",
                            rsPropData: a
                        })
                    }
                    var k = c.map(a, "profileId");
                    c.remove(k, function (a) {
                        return b.isUndefined(a)
                    });
                    var o = {
                        populations: k.join(","),
                        statistics: c.toArray(m).join(",")
                    }
                        , p = {
                            players: [{
                                userId: a[0].userId,
                                avatar: a[0].avatar.smallUrl,
                                username: a[0].username
                            }, {
                                userId: a[1].userId,
                                avatar: a[1].avatar.smallUrl,
                                username: a[1].username
                            }]
                        };
                    f(p).then(e).then(g)["catch"](function (a) {
                        if (!a.handled) {
                            var b = c.cloneDeep(l);
                            return p.categories = b,
                                p
                        }
                    })
                }
            }
    }
    e.$inject = ["$log", "$http", "$q", "$timeout", "statisticsService", "playerStatApiService", "rsMoleculeModalService"],
        b.module("g2w.statistics").service("statisticsCompareService", e)
}(window, window.angular, window._);

/** module("g2w.statistics").constant("REGIONS", e).constant("STATISTICS", d) */
!function (a, b, c) {
    "use strict";
    var d = {}
        , e = {
            NCSA: "ncsa",
            EMEA: "emea",
            APAC: "apac"
        };
    b.module("g2w.statistics").constant("REGIONS", e).constant("STATISTICS", d)
}(window, window.angular);

/** module("g2w.statistics").service("statisticsService", e) */
!function (a, b, c, d) {
    "use strict";
    function e(a, e, f, g, h, i, j, k, l, m, n) {
        function o(a, d) {
            var e, f = p(d);
            if (!c.isUndefined(f))
                switch (e = {},
                b.isDefined(a) && (e.value = a),
                e.serverKey = d,
                e = v(e, f),
                e.objectType) {
                    case "shootingweapontypes":
                        e.target = m.getByIndex("shootingweapontypes", e.objectIndex);
                        break;
                    case "weapons":
                        break;
                    case "operators":
                        e.target = m.getByFields("operators", ["fullIndex"], [e.objectIndex]);
                        break;
                    case "gadgets":
                        break;
                    case "skillranks":
                        e.target = m.getByIndex("skillranks", String(e.value))
                }
            return e
        }
        function p(a) {
            var b, d, e, f = a.split(":"), g = ["daily", "weekly", "monthly", "infinite"], h = f[f.length - 1];
            if (g.indexOf(h) >= 0 && (e = h,
                f = f.slice(0, f.length - 1),
                a = f.join(":")),
                c.isUndefined(w[a]) || (d = a,
                    b = w[a]),
                !c.isUndefined(b) && !c.isUndefined(d)) {
                var i = c.cloneDeep(b);
                return i.key = d,
                    c.isUndefined(e) || (i.period = e),
                    i
            }
        }
        function q(a, b) {
            var d;
            if (!c.isUndefined(a))
                if (c.isUndefined(a[b])) {
                    var e = b + ":infinite";
                    c.isUndefined(a[e]) || (d = a[e])
                } else
                    d = a[b];
            return d
        }
        function r(b) {
            var d = {}
                , e = function (a, b) {
                    if (c.has(a, "objectIndex")) {
                        var e = a.key.replace(":" + a.objectIndex, "");
                        d[e] = e
                    } else
                        d[b] = b
                }
                , f = function (b) {
                    if (c.has(b, "statId")) {
                        var d = p(b.statId);
                        d ? c.isUndefined(d.substats) ? e(d, b.statId) : c.forEach(d.substats, function (a) {
                            var b = p(a);
                            c.isUndefined(b) || e(b, a)
                        }) : a.warn("stat not found", b.statId)
                    }
                    c.forIn(b, function (a) {
                        c.isObject(a) && f(a)
                    })
                };
            return f(b),
                d
        }
        function s(a, e) {
            try {
                var f = function (e) {
                    c.forEach(e, function (e) {
                        if (!c.isUndefined(e) && !c.isNull(e)) {
                            if (!c.isUndefined(e.statId)) {
                                var g = q(a, e.statId)
                                    , h = o(g, e.statId);
                                e = v(h, e),
                                    b.isUndefined(e.label) && (e.label = e.oasisId)
                            }
                            if (!c.isUndefined(e.builder)) {
                                var i = [];
                                switch (e.substats && (c.forEach(a, function (a, b) {
                                    c.forEach(e.substats, function (d) {
                                        if (b.indexOf(d) >= 0 && !c.isUndefined(a) && !c.isNull(a)) {
                                            var e = o(a, b);
                                            c.isUndefined(e) || (c.isUndefined(e.objectType) ? i.push(e) : c.isUndefined(e.target) || i.push(e))
                                        }
                                    })
                                }),
                                    "ratio" === e.builder && c.forEach(e.substats, function (a) {
                                        var b = p(a);
                                        if (!c.isUndefined(b)) {
                                            var d = c.find(i, {
                                                key: b.key
                                            });
                                            c.isUndefined(d) && i.push(b)
                                        }
                                    })),
                                e.builder) {
                                    case "max":
                                        var j = t(i);
                                        e.target = j ? j.target : d,
                                            e.builderItems = i;
                                        break;
                                    case "order":
                                        var k = u(i);
                                        e.values = k,
                                            e.builderItems = i;
                                        break;
                                    case "ratio":
                                        var l = [];
                                        if (2 === i.length) {
                                            c.forEach(e.substats, function (a) {
                                                c.forEach(i, function (b) {
                                                    b.nameBase !== a && b.key !== a || l.push(b)
                                                })
                                            });
                                            var m = 0;
                                            (l[0].value || l[1].value) && (m = l[0].value ? l[1].value ? l[0].value / l[1].value : l[0].value : 0),
                                                e.value = m
                                        }
                                        e.builderItems = l
                                }
                            }
                            c.isObject(e) && f(e)
                        }
                    })
                }
                    , g = c.cloneDeep(e);
                f(g)
            } catch (h) {
                console.error("_mergeResultsToSchema", h)
            }
            return g
        }
        function t(a) {
            if (!a || !a.length)
                return null;
            var b = c.max(a, function (a) {
                return a.value
            });
            return c.isUndefined(b.serverKey) ? null : o(b.value, b.serverKey)
        }
        function u(a) {
            var b = c.sortBy(a, ["value"])
                , d = [];
            return c.forEachRight(b, function (a) {
                c.has(a, "value") && d.push(o(a.value, a.serverKey))
            }),
                d
        }
        function v(a, b) {
            var d = c.merge(b, a);
            return d
        }
        var w;
        return {
            loadDefinitions: function () {
                a.info("[statistics.service]", "loadDefinitions");
                var b = "https://ubistatic-a.akamaihd.net/0058/prod/assets/data/statistics.definitions.eb165e13.json";
                return e.get(b, {
                    cache: !0
                }).then(function (a) {
                    return w = a.data
                }, function (a) {
                    w = d
                })
            },
            isEmptyStat: function (a) {
                return b.isUndefined(a) || b.isUndefined(a.value) || 0 === a.value
            },
            merge: function (a, b) {
                return s(a, b)
            },
            extractStatisticsIds: function (a) {
                return r(a)
            },
            getStatisticsFromSchema: function (a, d) {
                function e(a) {
                    return j.getStatistics({
                        populations: d,
                        statistics: a
                    })
                }
                if (!b.isDefined(d) || !b.isDefined(a))
                    throw new Error("statisticsService::getStatisticsFromSchema implies to have a schema and at least one user");
                var f = r(a)
                    , g = c.toArray(f)
                    , h = []
                    , i = n.splitParamerters(g, 20);
                return c.forEach(i, function (a, b) {
                    h.push(e(c.map(a, function (a) {
                        return a.key
                    }).join(","), d))
                }),
                    n.qSequence(h, 350).then(function (d) {
                        var e = {};
                        return c.forEach(d, function (a) {
                            b.merge(e, a)
                        }),
                            c.forEach(e.results, function (b, d) {
                                var f = c.cloneDeep(a);
                                e[d.toUpperCase()] = s(b, f)
                            }),
                            e
                    })
            },
            getUsersStatistics: function (d, e) {
                function g(a) {
                    return l.getProgressions({
                        profile_ids: j.join(",")
                    }).then(function (d) {
                        return c.forEach(d.player_profiles, function (d) {
                            var e = d.profile_id
                                , f = c.find(a, {
                                    profileId: e
                                });
                            b.isDefined(f) && (f.clearance = d.level > 0 ? d.level : "-")
                        }),
                            a
                    })
                }
                function h(a) {
                    function g(a) {
                        return {
                            profile_ids: j.join(","),
                            season_id: e,
                            board_id: "pvp_ranked",
                            region_id: a
                        }
                    }
                    var h = {
                        rank1: k.get(g(i.NCSA)),
                        rank2: k.get(g(i.EMEA)),
                        rank3: k.get(g(i.APAC))
                    };
                    return f.all(h).then(function (a) {
                        function e(a) {
                            var e = a.profile_id
                                , f = c.find(d, {
                                    profileId: e
                                });
                            b.isDefined(f) && f.karmas.push(a)
                        }
                        return c.forEach(d, function (a) {
                            a.karmas = []
                        }),
                            c.forEach(a.rank1.players, e),
                            c.forEach(a.rank2.players, e),
                            c.forEach(a.rank3.players, e),
                            c.forEach(d, function (a) {
                                var d;
                                c.forEach(a.karmas, function (a) {
                                    b.isDefined(d) ? a.rank > d.rank && (d = a) : d = a
                                });
                                var e = {};
                                d.rank > 0 && (e.current = {
                                    type: "rank",
                                    value: c.floor(d.mmr),
                                    icon: d.rank
                                },
                                    d.rank > 0 && (e.previous = {
                                        type: "rank",
                                        value: c.floor(d.previous_rank_mmr),
                                        icon: d.rank - 1
                                    }),
                                    d.rank < 20 && (e.next = {
                                        type: "rank",
                                        value: c.floor(d.next_rank_mmr),
                                        icon: d.rank + 1
                                    })),
                                    a.ranks = e
                            }),
                            d
                    })
                }
                e = e ? e : -1,
                    a.debug("[statistics.service]", "getUsersStatistics", d);
                var j = c.map(d, "profileId");
                return c.remove(j, function (a) {
                    return b.isUndefined(a)
                }),
                    0 === c.size(j) ? (c.forEach(d, function (a) {
                        a.clearance = "-",
                            a.ranks = {}
                    }),
                        f.when(d)) : g(d).then(h)["catch"](function (a) {
                            return c.forEach(d, function (a) {
                                b.isUndefined(a.clearance) && (a.clearance = "-"),
                                    b.isUndefined(a.ranks) && (a.ranks = {})
                            }),
                                d
                        })
            }
        }
    }
    e.$inject = ["$log", "$http", "$q", "$timeout", "CONFIG", "REGIONS", "playerStatApiService", "r6KarmaApiService", "r6PlayerProfileApiService", "rsUtilModelService", "dataUtilsService"],
        b.module("g2w.statistics").service("statisticsService", e)
}(window, window.angular, window._);

/** module("g2w.watchlist").constant("WATCHLIST") */
!function (a, b, c) {
    "use strict";
    b.module("g2w.watchlist").constant("WATCHLIST", {
        max: 100
    })
}(window, window.angular);

/** module("g2w.watchlist").service("watchlistService", e) */
!function (a, b, c, d) {
    "use strict";
    function e(a, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t) {
        function u() {
            return a.debug("[watchlist.service]", "_getFriendsIds"),
                p.getRelationships().then(function (b) {
                    var d = c.filter(b.friends, {
                        state: "Friends"
                    })
                        , e = c.map(d, "pid");
                    return a.debug("[watchlist.service]", "_getFriendsIds done", e),
                        e
                })
        }
        function v(b) {
            return a.debug("[watchlist.service]", "_updateEntities"),
                m.updateProperty("web.user.watchlist." + y, "uids", b).then(function () {
                    return B = !1,
                        z = b,
                        a.debug("[watchlist.service]", "_updateEntities done", z),
                        f.$broadcast("watchlist.changed"),
                        z
                })
        }
        function w(d) {
            if (a.debug("[watchlist.service]", "_fetchProfiles"),
                0 === c.size(d))
                return e.when([]);
            var f = c.toArray(c.map(A, "userId"))
                , i = c.differenceWith(d, f);
            if (0 === c.size(i))
                return e.when(f);
            for (var j = function (a) {
                return n.search({
                    userId: a.join(",")
                }, [o.search])
            }, k = [], m = 0; m < i.length;) {
                var p = c.slice(i, m, m + 15);
                k.push(j(p)),
                    m += p.length
            }
            return l.qSequence(k, 250).then(function (d) {
                var e = [];
                return c.forEach(d, function (a) {
                    e.push.apply(e, a)
                }),
                    c.forEach(e, function (d) {
                        if (d[y]) {
                            var e = b.copy(d[y]);
                            e.avatar = e.avatar.smallUrl,
                                e.goToProfile = function () {
                                    var a = b.copy(h);
                                    a.userId = e.userId,
                                        g.go("playerstatistics.multiplayer", a),
                                        t.sendEvent("watchlistShowProfile"),
                                        r.closePanel("main")
                                }
                                ,
                                e.isWatched = !0,
                                e._sortField = e.username.toLowerCase();
                            var f = c.find(A, {
                                _sortField: e._sortField
                            });
                            b.isUndefined(f) ? A.splice(c.sortedIndexBy(A, e, "_sortField"), 0, e) : a.error("[watchlist.service]", "try to add a profile already in collection")
                        }
                    }),
                    a.debug("[watchlist.service]", "_fetchProfiles", "done", e.length),
                    c.toArray(c.map(A, "userId"))
            })
        }
        var x, y, z = [], A = [], B = !1, C = !1, D = !1;
        return {
            getMaxPlayer: function () {
                return k.max
            },
            init: function (b, c) {
                a.info("[watchlist.service]", "init", b, c);
                return D = !1,
                    A.splice(0, A.length),
                    x = b,
                    y = c,
                    z = m.getProperty("web.user.watchlist." + y, "uids") || [],
                    a.info("[watchlist.service]", "initialized", "uids", z),
                    f.$broadcast("watchlist.changed"),
                    e.when(z)
            },
            load: function () {
                if (a.debug("[watchlist.service]", "load", "already loading", C),
                    a.info("[watchlist.service]", "load"),
                    !C)
                    return C = !0,
                        D ? (a.info("[watchlist.service]", "skipping load"),
                            C = !1,
                            f.$broadcast("watchlist.changed"),
                            e.when()) : w(z).then(function () {
                                a.info("[watchlist.service]", "loaded"),
                                    D = !0,
                                    C = !1,
                                    t.setCustomDimension("watched", z.length),
                                    f.$broadcast("watchlist.changed")
                            })
            },
            reset: function () {
                a.info("[watchlist.service]", "reset"),
                    x = d,
                    y = d,
                    z.splice(0, z.length),
                    A.splice(0, A.length),
                    B = !1,
                    C = !1,
                    D = !1,
                    f.$broadcast("watchlist.changed")
            },
            list: A,
            getIsLoading: function () {
                return C
            },
            clear: function () {
                a.info("[watchlist.service]", "clear");
                var b = this;
                return e(function (c, d) {
                    m.clear("web.user.watchlist." + y).then(function () {
                        a.info("[watchlist.service]", "cleared"),
                            b.init(x, y).then(c, d)
                    }, d)
                })
            },
            isWatched: function (b) {
                var c = z.indexOf(b) >= 0;
                return a.info("[watchlist.service]", "isWatched", b, c),
                    c
            },
            add: function (d) {
                return a.info("[watchlist.service]", "add", y, d),
                    e(function (e, f) {
                        if (c.size(z) === k.max)
                            s.add("assets/tpl/components/notification.watchlist.max.tpl.html", {}),
                                t.sendEvent("watchlistFull"),
                                f(q.generateError("insufficientStorage", "watchlist.service", "add"));
                        else if (d === x)
                            f(q.generateError("methodNotAllowed", "watchlist.service", "add - self"));
                        else if (z.indexOf(d) >= 0)
                            f(q.generateError("methodNotAllowed", "watchlist.service", "add - already"));
                        else {
                            var g = b.copy(z);
                            g.push(d),
                                v(g).then(w).then(function (e) {
                                    var f = c.find(A, {
                                        userId: d
                                    });
                                    b.isDefined(f) && s.add("assets/tpl/components/notification.watchlist.added.tpl.html", {
                                        profile: {
                                            playername: f.username
                                        }
                                    }),
                                        t.sendEvent("addToWatchlist"),
                                        t.setCustomDimension("watched", g.length),
                                        a.info("[watchlist.service]", "added", y, d)
                                }).then(e)["catch"](f)
                        }
                    })
            },
            remove: function (d) {
                return a.info("[watchlist.service]", "remove", y, d),
                    e(function (e, g) {
                        if (z.indexOf(d) < 0)
                            g(q.generateError("methodNotAllowed", "watchlist.service", "remove - doesnt exist"));
                        else {
                            var h = z.indexOf(d)
                                , i = b.copy(z);
                            i.splice(h, 1),
                                v(i).then(function () {
                                    var g = c.find(A, {
                                        userId: d
                                    });
                                    b.isDefined(g) ? s.add("assets/tpl/components/notification.watchlist.removed.tpl.html", {
                                        profile: {
                                            playername: g.username
                                        }
                                    }) : a.warn("[watchlist.service]", "removed", d, "cant be found in profiles list (are u spamming key ?)");
                                    var h = A.indexOf(g);
                                    if (h >= 0) {
                                        A.splice(h, 1)
                                    }
                                    a.info("[watchlist.service]", "removed", y, d),
                                        t.sendEvent("removeFromWatchlist"),
                                        t.setCustomDimension("watched", i.length),
                                        z = i,
                                        f.$broadcast("watchlist.changed"),
                                        e()
                                }, g)
                        }
                    })
            },
            importFriends: function () {
                return a.info("[watchlist.service]", "importFriends", y),
                    u().then(function (a) {
                        var b = c.toArray(c.map(A, "userId"));
                        c.differenceWith(a, b);
                        return t.sendEvent("importFriends", a.length),
                            a
                    }).then(w).then(v).then(function (b) {
                        a.info("[watchlist.service]", "importFriends done", b.length),
                            b.length > 0 && (f.$broadcast("watchlist.changed"),
                                s.add("assets/tpl/components/notification.watchlist.imported.tpl.html", {
                                    "int": b.length
                                }))
                    })["catch"](function () { })
            }
        }
    }
    e.$inject = ["$log", "$q", "$rootScope", "$state", "$stateParams", "$timeout", "CONFIG", "WATCHLIST", "dataUtilsService", "entitiesService", "profileApiService", "profileParserService", "friendsApiService", "errorsService", "rsLayoutOffcanvasService", "notificationsService", "googleAnalyticsService"],
        b.module("g2w.watchlist").service("watchlistService", e)
}(window, window.angular, window._);

/** module("uplay.connect").constant("UPLAYCONNECT", d) */
!function (a, b, c) {
    "use strict";
    var d = {
        status: {
            failed: "FAILED"
        },
        requestTimeout: 1e4
    };
    b.module("uplay.connect").constant("UPLAYCONNECT", d)
}(window, window.angular);

/** module("uplay.connect").directive("uplayConnect", e) */
!function (a, b, c) {
    "use strict";
    function d(c, d, e, f) {
        if (c.addClass("uplay-connect"),
            b.isUndefined(a.location.origin)) {
            var g = a.location.protocol
                , h = a.location.hostname
                , i = a.location.port ? ":" + a.location.port : "";
            a.location.origin = g + "//" + h + i
        }
        var j = d.appIds.g2w
            , k = e.use()
            , l = a.location.origin + "/redirect.html"
            , m = d.uplayConnect.iframe + "?appId=" + j + "&lang=" + k + "&nextUrl=" + l;
        f.info("[uplay.connect.directive]", "iframeSrc", m),
            c.attr("src", m)
    }
    function e() {
        return {
            restrict: "E",
            replace: !0,
            template: "<iframe></iframe>",
            controller: d
        }
    }
    d.$inject = ["$element", "CONFIG", "$translate", "$log"],
        b.module("uplay.connect").directive("uplayConnect", e)
}(window, window.angular);

/** module("uplay.connect").run(d) */
!function (a, b, c) {
    "use strict";
    function d(a, c) {
        a.$on("$stateChangeError", function (a, d, e, f, g, h) {
            "permissions.service" === h.source && "isUserAllowed" === h.operation && "anonymous" === h.userType && (b.isUndefined(h.redirection) ? h.callback = c.showLogin : h.redirection.callback = c.showLogin)
        })
    }
    d.$inject = ["$rootScope", "uplayConnectService"],
        b.module("uplay.connect").run(d)
}(window, window.angular);

/** module("uplay.connect").service("uplayConnectService", d) */
!function (a, b, c) {
    "use strict";
    function d(c, d, e, f, g, h, i, j, k, l) {
        var m, n = a.uplay, o = {}, p = !1, q = "en-US";
        return a.redirectOnLogin = function () {
            m()
        }
            ,
            {
                init: function (a, e, h, i) {
                    return e = e || c.uplayConnect.genomeId,
                        h = h || q,
                        i = i || "",
                        f(function (c, f) {
                            if (b.isUndefined(n))
                                f(k.generateError("internal", "uplay.connect.service", "init"));
                            else {
                                var j = g(function () {
                                    f(k.generateError("requestTimeout", "uplay.connect.service", "init"));
                                }, d.requestTimeout);
                                if (p)
                                    c(o);
                                else
                                    try {
                                        n.init(a, e, h, i, function () {
                                            g.cancel(j),
                                                p = !0,
                                                c(o)
                                        })
                                    } catch (l) {
                                        f(k.generateError("internal", "uplay.connect.service", "init try-catch"))
                                    }
                            }
                        })
                },
                getTicket: function () {
                    return f(function (a, b) {
                        e.info("[uplay.connect.service]", "getTicket");
                        var c = g(function () {
                            e.info("[uplay.connect.service]", "getTicket", "timeout"),
                                b(k.generateError("requestTimeout", "uplay.connect.service", "getTicket"))
                        }, d.requestTimeout);
                        try {
                            n.getTicket(function (f) {
                                e.info("[uplay.connect.service]", "getTicket", "data", f),
                                    g.cancel(c),
                                    f.status !== d.status.failed ? (o = f,
                                        a(o)) : b(k.generateError("unauthorized", "uplay.connect.service", "getTicket"))
                            })
                        } catch (f) {
                            b(k.generateError("internal", "uplay.connect.service", "getTicket try-catch"))
                        }
                    })
                },
                logout: function () {
                    return f(function (a, b) {
                        var c = g(function () {
                            b(k.generateError("requestTimeout", "uplay.connect.service", "logout"))
                        }, d.requestTimeout);
                        n.logout(o.ticket, function () {
                            g.cancel(c),
                                o = {},
                                a(o)
                        })
                    })
                },
                setRedirectLogin: function (a) {
                    m = a
                },
                getUplayIframeUrl: function () {
                    var b = a.location.origin
                        , d = a.encodeURIComponent(b)
                        , e = c.uplayConnect.iframe + "?genomeId=" + c.uplayConnect.genomeId + "&appId=" + c.appIds.g2w + "&lang=" + i.use() + "&nextUrl=" + d + "/redirect.html";
                    return e
                },
                showLogin: function () {
                    var b = a.location.origin
                        , d = a.encodeURIComponent(b)
                        , e = c.uplayConnect.iframe + "?genomeId=" + c.uplayConnect.genomeId + "&appId=" + c.appIds.g2w + "&lang=" + i.use() + "&nextUrl=" + d + "/redirect.html";
                    j.$broadcast("loading.hide"),
                        l.openModal({
                            rsMoleculeModal: "assets/tpl/components/login.modal.tpl.html",
                            rsPropCloseOnBackdropClick: !0,
                            rsPropModalTitle: 195124,
                            rsPropData: e
                        })
                },
                reset: function () {
                    o = {}
                }
            }
    }
    d.$inject = ["CONFIG", "UPLAYCONNECT", "$log", "$q", "$timeout", "$state", "$translate", "$rootScope", "errorsService", "rsMoleculeModalService"],
        b.module("uplay.connect").service("uplayConnectService", d)
}(window, window.angular);

/** module("r6").provider("r6KarmaApiService", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        function a(a, d, e) {
            function f() {
                return c.baseUrl || d.config.baseUrl
            }
            var g = {
                config: c,
                get: function (e, g, h) {
                    return a.call("GET", f() + "/players", e, b.merge({}, d.config.headers, c.headers, h), g)
                }
            };
            return g
        }
        var c = {
            headers: {},
            baseUrl: ""
        }
            , d = {
                config: c,
                $get: ["httpService", "r6Service", "$log", a]
            };
        return d
    }
    b.module("r6").provider("r6KarmaApiService", d)
}(window, window.angular);

/** module("r6").provider("r6PlayerProfileApiService", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        function a(a, d, e) {
            function f() {
                var a = c.baseUrl || d.config.baseUrl;
                return a
            }
            var g = {
                config: c,
                getProgression: function (e, g, h) {
                    return a.call("GET", f() + "/playerprofile/progression", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getProgressions: function (e, g, h) {
                    return a.call("GET", f() + "/playerprofile/progressions", e, b.merge({}, d.config.headers, c.headers, h), g)
                }
            };
            return g
        }
        var c = {
            headers: {},
            baseUrl: ""
        }
            , d = {
                config: c,
                $get: ["httpService", "r6Service", "$log", a]
            };
        return d
    }
    b.module("r6").provider("r6PlayerProfileApiService", d)
}(window, window.angular);

/** module("r6").provider("r6Service", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        var a = (b.injector(["ng"]).get("$log"),
            {
                _headers: {}
            });
        Object.defineProperty(a, "headers", {
            get: function () {
                return a._headers
            },
            set: function (b) {
                a._headers = b
            }
        }),
            Object.defineProperty(a, "baseUrl", {
                get: function () {
                    return a._baseUrl
                },
                set: function (b) {
                    a._baseUrl = b
                }
            });
        var c = {
            config: a,
            $get: ["$log", function (b) {
                var c = {
                    config: a
                };
                return c
            }
            ]
        };
        return c
    }
    b.module("r6").provider("r6Service", d)
}(window, window.angular);

/** module("rdv").config(e) */
!function (a, b, c, d) {
    "use strict";
    function e(a) { }
    e.$inject = ["CONFIG"],
        b.module("rdv").config(e)
}(window, window.angular, window._);

/** module("rdv").service("rdvConfigService", d) */
!function (a, b, c) {
    "use strict";
    function d(a, b, c, d, e) {
        return {
            setPlatformType: function (f) {
                a.info("[rdv.config.service]", "setPlatformType", f);
                var g = e.ubiservices.baseUrl + "/v1/spaces/" + e.spaceIds[f] + "/sandboxes/" + e.sandboxes[f];
                b.config.baseUrl = g + "/r6playerprofile",
                    c.config.baseUrl = g + "/r6karma",
                    d.config.baseUrl = g + "/playerstats2"
            }
        }
    }
    d.$inject = ["$log", "r6PlayerProfileApiService", "r6KarmaApiService", "playerStatApiService", "CONFIG"],
        b.module("rdv").service("rdvConfigService", d)
}(window, window.angular);

/** module("rdv").provider("playerStatApiService", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        function a(a, d, e) {
            function f() {
                return c.baseUrl || d.config.baseUrl
            }
            var g = {
                config: c,
                getStatistics: function (e, g, h) {
                    return a.call("GET", f() + "/statistics", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getLeaderboard: function (e, g, h) {
                    return a.call("GET", f() + "/leaderboards/:leaderboardName/:period", e, b.merge({}, d.config.headers, c.headers, h), g)
                }
            };
            return g
        }
        var c = {
            headers: {},
            baseUrl: ""
        }
            , d = {
                config: c,
                $get: ["httpService", "rdvService", "$log", a]
            };
        return d
    }
    b.module("rdv").provider("playerStatApiService", d)
}(window, window.angular);

/** module("rdv").provider("rdvService", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        var a = (b.injector(["ng"]).get("$log"),
            {
                _headers: {}
            });
        Object.defineProperty(a, "headers", {
            get: function () {
                return a._headers
            },
            set: function (b) {
                a._headers = b
            }
        }),
            Object.defineProperty(a, "baseUrl", {
                get: function () {
                    return a._baseUrl
                },
                set: function (b) {
                    a._baseUrl = b
                }
            });
        var c = {
            config: a,
            $get: ["$log", function (b) {
                var c = {
                    config: a
                };
                return c
            }
            ]
        };
        return c
    }
    b.module("rdv").provider("rdvService", d)
}(window, window.angular);

/** module("ubiservices").provider("applicationusedApiService", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        function a(a, d, e) {
            function f() {
                return c._baseUrl || d.config.baseUrl
            }
            var g = {
                config: c,
                getMeProfileApplications: function (e, g, h) {
                    return a.call("GET", f() + "/v3/profiles/me/applications", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getProfileApplicationsByProfile: function (e, g, h) {
                    return a.call("GET", f() + "/v3/profiles/:profileId/applications", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getProfileApplicationsByProfileList: function (e, g, h) {
                    return a.call("GET", f() + "/v3/profiles/applications", e, b.merge({}, d.config.headers, c.headers, h), g)
                }
            };
            return g
        }
        var c = {
            headers: {},
            baseUrl: ""
        }
            , d = {
                config: c,
                $get: ["httpService", "ubiservicesService", "$log", a]
            };
        return d
    }
    b.module("ubiservices").provider("applicationusedApiService", d)
}(window, window.angular);

/** module("ubiservices").provider("entitiesApiService", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        function a(a, d, e) {
            function f() {
                return c._baseUrl || d.config.baseUrl
            }
            var g = {
                config: c,
                getProfilesEntitiesSearch: function (e, g, h) {
                    return a.call("GET", f() + "/v2/profiles/entities", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getProfilesEntityById: function (e, g, h) {
                    return a.call("GET", f() + "/v2/profiles/entities/:entityId", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                updateProfilesEntity: function (e, g, h) {
                    return a.call("PUT", f() + "/v2/profiles/entities/:entityId", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                deleteProfilesEntity: function (e, g, h) {
                    return a.call("DELETE", f() + "/v2/profiles/entities/:entityId", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getProfileEntitiesSearch: function (e, g, h) {
                    return a.call("GET", f() + "/v2/profiles/:profileId/entities", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                createProfilesEntity: function (e, g, h) {
                    return a.call("POST", f() + "/v2/profiles/:profileId/entities", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getSpacesEntitiesSearch: function (e, g, h) {
                    return a.call("GET", f() + "/v2/spaces/entities", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getSpacesEntity: function (e, g, h) {
                    return a.call("GET", f() + "/v2/spaces/entities/:entityId", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getSpaceEntitiesSearch: function (e, g, h) {
                    return a.call("GET", f() + "/v2/spaces/:spaceId/entities", e, b.merge({}, d.config.headers, c.headers, h), g)
                }
            };
            return g
        }
        var c = {
            headers: {},
            baseUrl: ""
        }
            , d = {
                config: c,
                $get: ["httpService", "ubiservicesService", "$log", a]
            };
        return d
    }
    b.module("ubiservices").provider("entitiesApiService", d)
}(window, window.angular);

/** module("ubiservices").provider("friendsApiService", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        function a(a, d, e) {
            function f() {
                return c._baseUrl || d.config.baseUrl
            }
            var g = {
                config: c,
                getRelationships: function (e, g, h) {
                    return a.call("GET", f() + "/v2/profiles/me/friends", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                sendInvite: function (e, g, h) {
                    return a.call("POST", f() + "/v2/profiles/me/friends", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                acceptInvite: function (e, g, h) {
                    return a.call("PUT", f() + "/v2/profiles/me/friends/:friendPid", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                clearRelationship: function (e, g, h) {
                    return a.call("DELETE", f() + "/v2/profiles/me/friends/:friendPid", e, b.merge({}, d.config.headers, c.headers, h), g)
                }
            };
            return g
        }
        var c = {
            headers: {},
            baseUrl: ""
        }
            , d = {
                config: c,
                $get: ["httpService", "ubiservicesService", "$log", a]
            };
        return d
    }
    b.module("ubiservices").provider("friendsApiService", d)
}(window, window.angular);

/** module("ubiservices").provider("profileApiService", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        function a(a, d, e) {
            function f() {
                return c._baseUrl || d.config.baseUrl
            }
            var g = {
                config: c,
                getProfile: function (e, g, h) {
                    return a.call("GET", f() + "/v2/profiles/:profileId", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                search: function (e, g, h) {
                    return a.call("GET", f() + "/v2/profiles", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getSessionProfile: function (e, g, h) {
                    return a.call("GET", f() + "/v2/profiles/me", e, b.merge({}, d.config.headers, c.headers, h), g)
                }
            };
            return g
        }
        var c = {
            headers: {},
            baseUrl: ""
        }
            , d = {
                config: c,
                $get: ["httpService", "ubiservicesService", "$log", a]
            };
        return d
    }
    b.module("ubiservices").provider("profileApiService", d)
}(window, window.angular);

/** module("ubiservices").provider("ubiservicesService", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        var a = (b.injector(["ng"]).get("$log"),
            {
                _headers: {}
            });
        Object.defineProperty(a, "headers", {
            get: function () {
                return a._headers
            },
            set: function (b) {
                a._headers = b
            }
        }),
            Object.defineProperty(a, "baseUrl", {
                get: function () {
                    return a._baseUrl
                },
                set: function (b) {
                    a._baseUrl = b
                }
            });
        var c = {
            config: a,
            $get: ["$log", function (b) {
                var c = {
                    config: a
                };
                return c
            }
            ]
        };
        return c
    }
    b.module("ubiservices").provider("ubiservicesService", d)
}(window, window.angular);

/** module("ubiservices").provider("uplaywinApiService", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        function a(a, d, e) {
            function f() {
                return c._baseUrl || d.config.baseUrl
            }
            var g = {
                config: c,
                getProfileActions: function (e, g, h) {
                    return a.call("GET", f() + "/v1/profiles/:profileId/actions", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                postProfileActions: function (e, g, h) {
                    return a.call("POST", f() + "/v1/profiles/:profileId/actions", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getSpaceActions: function (e, g, h) {
                    return a.call("GET", f() + "/v1/spaces/:spaceId/actions", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getProfileRewards: function (e, g, h) {
                    return a.call("GET", f() + "/v1/profiles/:profileId/rewards", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                postProfileRewards: function (e, g, h) {
                    return a.call("POST", f() + "/v1/profiles/:profileId/rewards", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getSpaceRewards: function (e, g, h) {
                    return a.call("GET", f() + "/v1/spaces/:spaceId/rewards", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getUplayProfileActions: function (e, g, h) {
                    return a.call("GET", f() + "/v1/profiles/:profileId/uplay/actions", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                postUplayProfileActions: function (e, g, h) {
                    return a.call("POST", f() + "/v1/profiles/:profileId/uplay/actions", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getUplaySpaceActions: function (e, g, h) {
                    return a.call("GET", f() + "/v1/spaces/:spaceId/uplay/actions", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getUplayProfileRewards: function (e, g, h) {
                    return a.call("GET", f() + "/v1/profiles/:profileId/uplay/rewards", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                postUplayProfileRewards: function (e, g, h) {
                    return a.call("POST", f() + "/v1/profiles/:profileId/uplay/rewards", e, b.merge({}, d.config.headers, c.headers, h), g)
                },
                getUplaySpaceRewards: function (e, g, h) {
                    return a.call("GET", f() + "/v1/spaces/:spaceId/uplay/rewards", e, b.merge({}, d.config.headers, c.headers, h), g)
                }
            };
            return g
        }
        var c = {
            headers: {},
            baseUrl: ""
        }
            , d = {
                config: c,
                $get: ["httpService", "ubiservicesService", "$log", a]
            };
        return d
    }
    b.module("ubiservices").provider("uplaywinApiService", d)
}(window, window.angular);

/** module("ubiservices").provider("usersApiService", d) */
!function (a, b, c) {
    "use strict";
    function d() {
        function a(a, d, e) {
            function f() {
                return c._baseUrl || d.config.baseUrl
            }
            var g = {
                config: c,
                getProfiles: function (e, g, h) {
                    return a.call("GET", f() + "/v2/users/:userId/profiles", e, b.merge({}, d.config.headers, c.headers, h), g)
                }
            };
            return g
        }
        var c = {
            headers: {},
            baseUrl: ""
        }
            , d = {
                config: c,
                $get: ["httpService", "ubiservicesService", "$log", a]
            };
        return d
    }
    b.module("ubiservices").provider("usersApiService", d)
}(window, window.angular);

/** module("data").config(d) */
!function (a, b, c) {
    "use strict";
    function d(a, c, d, e) {
        b.injector(["ng"]).get("$log");
        c.config.headers["Ubi-AppId"] = a.appIds.g2w,
            d.config.baseUrl = a.ubiservices.baseUrl
    }
    d.$inject = ["CONFIG", "httpServiceProvider", "ubiservicesServiceProvider", "r6ServiceProvider"],
        b.module("data").config(d)
}(window, window.angular);

/** module("data").provider("httpService", e) */
!function (a, b, c, d) {
    "use strict";
    function e() {
        var a = (b.injector(["ng"]).get("$log"),
            {
                _headers: {}
            });
        Object.defineProperty(a, "headers", {
            get: function () {
                return a._headers
            },
            set: function (b) {
                a._headers = b
            }
        });
        var d = {
            config: a,
            $get: ["$http", "$q", "$log", "parsersService", function (d, e, f, g) {
                function h(a, c) {
                    var d = a.match(/([a-z]+\:\/\/)/);
                    if (b.isUndefined(d) || d.length <= 0)
                        throw "Invalid url format: " + a;
                    var e = d[1];
                    a = a.replace(e, "");
                    var f = j(a);
                    return f = i(f, c),
                        e + k(f)
                }
                function i(a, d) {
                    var e = [];
                    return c.forEach(a, function (a) {
                        if (b.isDefined(a) && "" !== a && 0 === a.indexOf(":")) {
                            var c = a.replace(":", "")
                                , f = d[c];
                            delete d[c],
                                a = f
                        }
                        e.push(a)
                    }),
                        e
                }
                function j(a) {
                    var d = a.split("/")
                        , e = [];
                    return c.forEach(d, function (a) {
                        b.isDefined(a) && "" !== a && e.push(a)
                    }),
                        e
                }
                function k(a) {
                    var b = a.join("/");
                    return b
                }
                var l = {
                    config: a,
                    call: function (f, i, j, k, l) {
                        var m = {
                            method: f,
                            url: h(i, j),
                            headers: b.merge({}, k, a.headers)
                        };
                        return "GET" === f ? m.params = j || {} : m.data = j || {},
                            e(function (a, e) {
                                d(m).then(function (d) {
                                    var e;
                                    if (b.isDefined(l) && c.isString(l) && (e = g.defaults(l)),
                                        b.isDefined(l) && c.isArray(l) && l.length > 0 && (e = l),
                                        b.isDefined(e)) {
                                        var f;
                                        e.forEach(function (a) {
                                            f = b.isDefined(f) ? b.merge({}, f, a(d.data, j)) : a(d.data, j)
                                        }),
                                            a(f)
                                    } else
                                        a(d.data)
                                }, function (a) {
                                    e(a)
                                })
                            })
                    }
                };
                return l
            }
            ]
        };
        return d
    }
    b.module("data").provider("httpService", e)
}(window, window.angular, window._);

/** module("data").service("parsersService", d) */
!function (a, b, c) {
    "use strict";
    function d(a) {
        var c = this;
        return b.extend(c, {
            "default": function (a) {
                return function (b, c) {
                    return b[a]
                }
            },
            defaults: function (a) {
                return [c["default"](a)]
            }
        })
    }
    d.$inject = ["$log"],
        b.module("data").service("parsersService", d)
}(window, window.angular);

/** module("data").service("dataUtilsService", d) */
!function (a, b, c) {
    "use strict";
    function d(a, b, c) {
        return {
            qSequence: function (a, d) {
                var e, f;
                return _.isArray(a) ? (e = [],
                    f = a) : (e = {},
                        f = _.toArray(a)),
                    f.reduce(function (c, f, g, h) {
                        return c.then(function (c) {
                            return b(function () {
                                return f.then(function (b) {
                                    if (_.isArray(e))
                                        e.push(b);
                                    else {
                                        var c;
                                        _.forEach(a, function (a, b) {
                                            a === f && (c = b)
                                        }),
                                            e[c] = b
                                    }
                                    return e
                                })
                            }, d)
                        })
                    }, c.when(e))
            },
            splitParamerters: function (a, b) {
                for (var c = [], d = a.length; d > 0;) {
                    var e = _.take(a, b);
                    a = _.slice(a, e.length),
                        d = a.length,
                        c.push(e)
                }
                return c
            }
        }
    }
    d.$inject = ["$log", "$timeout", "$q"],
        b.module("data").service("dataUtilsService", d)
}(window, window.angular);

/** module("errors").constant("ERRORS", d) */
!function (a, b, c) {
    "use strict";
    var d = {
        httpStatus: {
            requestTimeout: {
                code: 408,
                message: "request timeout"
            },
            unauthorized: {
                code: 401,
                message: "authentication is required and has failed"
            },
            badRequest: {
                code: 400,
                message: "client error"
            },
            notFound: {
                code: 404,
                message: "not found"
            },
            internal: {
                code: 500,
                message: "internal Server Error"
            },
            insufficientStorage: {
                code: 507,
                message: "Insufficient storage"
            },
            methodNotAllowed: {
                code: 405,
                message: "Method Not Allowed"
            }
        }
    };
    b.module("errors").constant("ERRORS", d)
}(window, window.angular);

/** module("errors").run(e) */
!function (a, b, c, d) {
    "use strict";
    function e(a, b, c, d) {
        a.$on("$stateChangeError", function (a, b, e, f, g, h) {
            d.warn("$stateChangeError", h),
                c.handleError(h)
        })
    }
    e.$inject = ["$rootScope", "$state", "errorsService", "$log"],
        b.module("errors").run(e)
}(window, window.angular, window._);

/** module("errors").service("errorsService", d) */
!function (a, b, c) {
    "use strict";
    function d(a, d, e, f, g) {
        return {
            handleError: function (a) {
                var f = !1;
                if (b.isUndefined(a))
                    return !1;
                var g = a.code || a.status
                    , h = a.redirection || {};
                h.params = h.params || {},
                    h.options = h.options || {},
                    h.reload === !0 && (h.options.reload = !0),
                    a.redirection || (h.name = g ? g.toString() : c);
                var i = e.get(h.name);
                return b.isDefined(i) && null !== i ? (f = !0,
                    a.handled = !0,
                    d.debug("[errors.service]", "redirection go", h),
                    e.go(h.name, h.params, h.options).then(function () {
                        b.isDefined(h.callback) && h.callback()
                    })) : b.isDefined(a.callback) && a.callback(),
                    f
            },
            generateError: function (c, d, e, g) {
                var h = b.copy(a.httpStatus[c]);
                if (b.isUndefined(h))
                    throw new ReferenceError("The errorName '" + c + "' do not exist.");
                b.isDefined(d) && (h.source = d),
                    b.isDefined(e) && (h.operation = e),
                    g && (h.redirection = g);
                var i = f.get("permissionsService");
                return h.userType = i.getUserType(),
                    h
            }
        }
    }
    d.$inject = ["ERRORS", "$log", "$state", "$injector", "rsMoleculeModalService"],
        b.module("errors").service("errorsService", d)
}(window, window.angular);

/** module("events").provider("eventsService", e) */
!function (a, b, c, d) {
    "use strict";
    function e() {
        var a = (b.injector(["ng"]).get("$log"),
            [])
            , d = {};
        return {
            addListener: function (b, c, d) {
                a.push({
                    caller: b,
                    eventName: c,
                    callback: d
                })
            },
            $get: ["$log", "$rootScope", function (b, e) {
                function f(a, b, f) {
                    d[b] || (d[b] = []),
                        d[b].push({
                            caller: a,
                            eventName: b,
                            callback: f,
                            cleanUpFn: e.$on(b, f)
                        }),
                        c.has(a, "$on") && a.$on("$destroy", function () {
                            g(a, b, f)
                        })
                }
                function g(a, b, e) {
                    var f = d[b]
                        , g = [];
                    f.forEach(function (c) {
                        c.caller === a && c.eventName === b && c.callback === e && (g.push(c),
                            c.cleanUpFn())
                    }),
                        c.remove(f, function (a) {
                            return !c.isUndefined(c.find(g, a))
                        })
                }
                a.forEach(function (a) {
                    f(a.caller, a.eventName, a.callback)
                });
                var h = {
                    addListener: function (a, b, c) {
                        f(a, b, c)
                    },
                    removeListener: function (a, b, c) {
                        g(a, b, c)
                    },
                    dispatch: function (a, b, c) {
                        e.$broadcast(b, c)
                    }
                };
                return h
            }
            ]
        }
    }
    b.module("events").provider("eventsService", e)
}(window, window.angular, window._);

/** module("loading").directive("loading", e) */
!function (a, b, c) {
    "use strict";
    function d(a, b) {
        function c() {
            e.show = !0
        }
        function d() {
            e.show = !1
        }
        b.addClass("loading");
        var e = this
            , f = a.$on("loading.show", c)
            , g = a.$on("loading.hide", d);
        a.$on("$destroy", function () {
            f(),
                g()
        })
    }
    function e(a, b, c, e) {
        return {
            restrict: "E",
            replace: !0,
            transclude: !0,
            template: '<div ng-show="vm.show"><ng-transclude></ng-transclude></div>',
            controller: d,
            controllerAs: "vm",
            bindToController: !0
        }
    }
    d.$inject = ["$scope", "$element"],
        e.$inject = ["$log", "$compile", "$window", "$timeout"],
        b.module("loading").directive("loading", e)
}(window, window.angular);

/** module("loading").run(d) */
!function (a, b, c) {
    "use strict";
    function d(a, b) {
        b.$on("$stateChangeStart", function () {
            b.$broadcast("loading.show")
        }),
            b.$on("$stateChangeSuccess", function () {
                b.$broadcast("loading.hide")
            })
    }
    d.$inject = ["$log", "$rootScope"],
        b.module("loading").run(d)
}(window, window.angular);

/** module("log").config(d) */
!function (a, b, c) {
    "use strict";
    function d(a, b) {
        a.debugEnabled("debug" === b.logLevel || "transport" === b.logLevel || "all" === b.logLevel)
    }
    d.$inject = ["$logProvider", "CONFIG"],
        b.module("log").config(d)
}(window, window.angular);

/** module("log").constant("LOG", d) */
!function (a, b, c) {
    "use strict";
    var d = {
        levels: ["none", "log", "info", "warn", "error", "debug", "transport", "all"]
    };
    b.module("log").constant("LOG", d)
}(window, window.angular);

/** module("log").config(e) */
!function (a, b, c, d) {
    "use strict";
    function e(a, b, d) {
        a.decorator("$log", ["$delegate", function (a) {
            function e(a) {
                var b = i.indexOf(h)
                    , c = i.indexOf(a);
                return b >= c
            }
            var f = {}
                , g = {}
                , h = d.logLevel
                , i = b.levels;
            i.indexOf(h) === -1 && (h = i[0]);
            for (var j in a) {
                var k = a[j];
                a[j] = c.wrap(k, function (b) {
                    if (e(j)) {
                        var c = Array.prototype.slice.call(arguments, 1);
                        b.apply(a, c)
                    }
                })
            }
            return a.transportRequest = function (b) {
                if (e("transport")) {
                    var c = [].slice.call(arguments);
                    c.unshift("transport"),
                        a.debug.apply(null, c),
                        a.time(b.url),
                        a.count(b.url)
                }
            }
                ,
                a.transportResponse = function (b, c) {
                    if (e("transport")) {
                        var d = [].slice.call(arguments);
                        d.unshift("transport"),
                            a.debug.apply(null, d),
                            a.timeEnd(b.url),
                            a.countOk(b.url)
                    }
                }
                ,
                a.transportError = function (b, c) {
                    if (e("transport")) {
                        var d = [].slice.call(arguments);
                        d.unshift("transport"),
                            a.debug.apply(null, d),
                            a.timeEnd(b.url),
                            a.countError(b.url)
                    }
                }
                ,
                a.logGroup = function () {
                    var a = [].slice.call(arguments)
                        , b = a.shift();
                    console.groupCollapsed(b);
                    for (var c in a)
                        console.log(a[c]);
                    console.groupEnd()
                }
                ,
                a.profile = function (a) {
                    console.profile(a)
                }
                ,
                a.profileEnd = function (a) {
                    console.profileEnd(a)
                }
                ,
                a.timeStamp = function (a) {
                    console.timeStamp(a)
                }
                ,
                a.trace = function () {
                    console.trace(arguments)
                }
                ,
                a.time = function (a) {
                    console.time(a),
                        g[a] || (g[a] = {
                            total: 0,
                            count: 0,
                            current: {}
                        }),
                        g[a].current.start = (new Date).getTime()
                }
                ,
                a.timeEnd = function (a) {
                    console.timeEnd(a),
                        g[a] && (g[a].current.end = (new Date).getTime(),
                            g[a].count++ ,
                            g[a].total += g[a].current.end - g[a].current.start,
                            g[a].avg = g[a].total / g[a].count)
                }
                ,
                a.count = function (a) {
                    console.count(a),
                        f[a] = f[a] ? f[a] + 1 : 1
                }
                ,
                a.countOk = function (b) {
                    a.count(b + "-OK")
                }
                ,
                a.countError = function (b) {
                    a.count(b + "-ERROR")
                }
                ,
                a.countWarn = function (b) {
                    a.count(b + "-WARN")
                }
                ,
                a.stats = function () {
                    a.statsCount(),
                        a.statsTime()
                }
                ,
                a.statsCount = function () {
                    console.groupCollapsed("count stats");
                    for (var a in f)
                        console.log(a, ":", f[a]);
                    console.groupEnd()
                }
                ,
                a.statsCountFlush = function () {
                    f = {}
                }
                ,
                a.statsTime = function () {
                    console.groupCollapsed("time stats");
                    for (var a in g)
                        console.log(a, ":", g[a].avg ? g[a].avg + "ms" : "-");
                    console.groupEnd()
                }
                ,
                a.statsTimeFlush = function () {
                    g = {}
                }
                ,
                a
        }
        ])
    }
    e.$inject = ["$provide", "LOG", "CONFIG"],
        b.module("log").config(e)
}(window, window.angular, window._);

/** module("log").factory("debugInterceptor", d), */
/** module("log").config(e) */
!function (a, b, c) {
    "use strict";
    function d(a, b) {
        var c = {
            request: function (a) {
                return b.transportRequest(a),
                    a
            },
            requestError: function (b) {
                return a.reject(b)
            },
            response: function (a) {
                return b.transportResponse(a.config, a),
                    a
            },
            responseError: function (c) {
                return b.transportError(c.config, c),
                    a.reject(c)
            }
        };
        return c
    }
    function e(a) {
        a.interceptors.push("debugInterceptor")
    }
    d.$inject = ["$q", "$log"],
        e.$inject = ["$httpProvider"],
        b.module("log").factory("debugInterceptor", d),
        b.module("log").config(e)
}(window, window.angular);

/** module("permissions").service("permissionsService", d) */
!function (a, b, c) {
    "use strict";
    function d(a, c, d, e, f) {
        function g() {
            var a, f, g = c.defer(), j = d.next, k = d.toParams;
            b.isDefined(j) && b.isDefined(j.data) && b.isDefined(j.data.permissions) && (a = j.data.permissions,
                f = a.redirection[h],
                f && (f.params = d.toParams || {}));
            var l;
            return a && (a.except && a.except.indexOf(h) !== -1 && (l = e.generateError("unauthorized", "permissions.service", "isUserAllowed", f)),
                a.only && a.only.indexOf(h) === -1 && (l = e.generateError("unauthorized", "permissions.service", "isUserAllowed", f))),
                b.isUndefined(l) ? g.resolve() : (i = {
                    name: j.name,
                    params: k
                },
                    g.reject(l)),
                g.promise
        }
        var h, i;
        return {
            getRejectedState: function () {
                return i
            },
            setUserType: function (a) {
                h = a
            },
            getUserType: function () {
                return h
            },
            isUserAllowed: function () {
                return g()
            }
        }
    }
    d.$inject = ["$log", "$q", "$state", "errorsService", "rsMoleculeModalService"],
        b.module("permissions").service("permissionsService", d)
}(window, window.angular);

/** module("g2w.router").config(d) */
!function (a, b, c) {
    "use strict";
    function d(a, b, c) {
        a.html5Mode(!0),
            b.strictMode(!1),
            c.decorator("$state", ["$delegate", "$rootScope", function (a, b) {
                return b.$on("$stateChangeStart", function (b, c, d) {
                    a.next = c,
                        a.toParams = d
                }),
                    a
            }
            ])
    }
    d.$inject = ["$locationProvider", "$urlMatcherFactoryProvider", "$provide"],
        b.module("g2w.router").config(d)
}(window, window.angular);

/** module("g2w.index").config(k) */
!function (a, b, c, d) {
    "use strict";
    function e(a, b, c, d, e, f, g) {
        function h() {
            var a = e.parseSessionSync();
            return g.setUserId(a.userId),
                f.setUserType("player"),
                c.config.headers["Ubi-SessionId"] = a.id,
                c.config.headers.Authorization = "Ubi_v1 t=" + a.ticket,
                b.when()
        }
        function i(a) {
            return g.setCustomDimension("dataPlatform", a.autoPlatform),
                b.when(a)
        }
        return a.info("[index.config]", "*resolve*", "session"),
            f.setUserType("anonymous"),
            g.setCustomDimension("appVersion", d),
            e.init().then(e.getTicket).then(h).then(e.getProfiles).then(e.getEntity).then(e.getApplications).then(e.parseSession).then(i)["catch"](function (b) {
                a.info("[index.config]", "session resolve", b);
                var c = "uplay.connect.service" === b.source && "getTicket" === b.operation
                    , d = "index.session.service" === b.source && "getProfiles" === b.operation;
                (c || d) && f.setUserType("anonymous")
            })
    }
    function f(a, b, c) {
        return b.info("[index.config]", "*resolve*", "locale"),
            c.initSession(a).then(c.init).then(c.redirect).then(c.parseLocalizationData)
    }
    function g(a, d, e, f, g, h) {
        function i(a) {
            var d = c.find(a.actions, {
                id: "10"
            });
            return b.isDefined(d) && d.isJustCompleted && (g.add("assets/tpl/components/notification.action.club.tpl.html", {
                stat: d.name,
                image: d.image
            }),
                h.sendEvent("uplayAction")),
                a
        }
        return e.info("[index.config]", "*resolve*", "platform"),
            f.initSession(d).then(f.init).then(f.redirect).then(f.parsePlatform).then(f.processActions).then(i)
    }
    function h(a, b, c, d, e) {
        return a.info("[index.config]", "sessionPlatformProfileResolve"),
            e.getPlatformProfile()
    }
    function i(a, b, c, d) {
        d.setPlatformType(b.type)
    }
    function j(a, b, c, d) {
        return d.init(a.userId, b.type)
    }
    function k(a, b) {
        b.when("/", ["$injector", function (a) {
            var b = a.get("localizationService")
                , c = "/" + b.getUrlLocale();
            return c
        }
        ]),
            a.state("root", {
                url: "",
                "abstract": !0,
                templateUrl: "assets/tpl/root.tpl.html",
                controller: "indexController",
                controllerAs: "indexCtrl",
                resolve: {
                    tagCommander: ["tagCommanderService", function (a) {
                        return a.reset()
                    }
                    ],
                    session: e
                }
            }).state("locale", {
                parent: "root",
                url: "/:locale",
                "abstract": !0,
                views: {
                    "header@root": {
                        templateUrl: "assets/tpl/header.tpl.html"
                    },
                    footer: {
                        templateUrl: "assets/tpl/footer.tpl.html"
                    }
                },
                resolve: {
                    locale: f
                }
            }).state("platform", {
                parent: "locale",
                url: "/:platform",
                "abstract": !0,
                views: {
                    "header@root": {
                        templateUrl: "assets/tpl/header.tpl.html"
                    }
                },
                resolve: {
                    isAllowed: ["locale", "permissionsService", function (a, b) {
                        return b.isUserAllowed()
                    }
                    ],
                    platform: g,
                    sessionPlatformProfile: h,
                    rdvServicesConfig: i,
                    tagCommander: ["session", "rdvServicesConfig", "tagCommanderService", function (a, b, c) {
                        return c.init(a)
                    }
                    ],
                    watchlist: j
                }
            })
    }
    e.$inject = ["$log", "$q", "httpService", "VERSION", "sessionService", "permissionsService", "googleAnalyticsService"],
        f.$inject = ["session", "$log", "localizationService"],
        g.$inject = ["isAllowed", "session", "$log", "platformsService", "notificationsService", "googleAnalyticsService"],
        h.$inject = ["$log", "session", "locale", "platform", "platformsService"],
        i.$inject = ["session", "platform", "sessionPlatformProfile", "rdvConfigService"],
        j.$inject = ["session", "platform", "rdvServicesConfig", "watchlistService"],
        k.$inject = ["$stateProvider", "$urlRouterProvider"],
        b.module("g2w.index").config(k)
}(window, window.angular, window._);

/** module("g2w.index").constant("INDEX", d) */
!function (a, b, c) {
    "use strict";
    var d = {
        defaultStateName: "playerstatistics.multiplayer"
    };
    b.module("g2w.index").constant("INDEX", d)
}(window, window.angular);

/** module("g2w.index").controller("indexController", e) */
!function (a, b, c, d) {
    "use strict";
    function e(d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w) {
        function x() {
            B.watchlist.isLoading = t.getIsLoading()
        }
        function y() {
            g.params.locale && (B.langClass = "lang-" + g.params.locale);
            var a = l.parseSessionSync()
                , e = n.parsePlatformSync()
                , f = m.parseLocalizationSync();
            if (B.localeCode = f.code,
                B.logged = b.isDefined(a.id),
                B.username = "",
                B.avatarUrl = "",
                b.isDefined(e) && b.isDefined(e.type) && (B.platform = b.copy(e.type)),
                c.isEmpty(a) ? (C.myStats.state = "playerstatistics",
                    C.rewards.state = "rewards({seasonId:" + i.rewardsSeason + "})") : (C.myStats.state = 'playerstatistics({userId:"' + a.userId + '"})',
                        C.rewards.state = "rewardsLogged({seasonId:" + i.rewardsSeason + "})"),
                d.debug("[index.service]", "platform", B.platform),
                B.logged) {
                B.headerNavigation.secondary = [C.search, C.watchlist],
                    B.sideNavigation.secondary = [C.logout];
                var h;
                h = b.isDefined(e) && b.isDefined(e.type) ? a.profiles[e.type] : a.profiles.uplay,
                    b.isUndefined(h) && (h = c.head(c.toArray(a.profiles))),
                    b.isDefined(h) && (B.username = h.username,
                        B.avatarUrl = h.avatar.smallUrl)
            } else
                B.headerNavigation.secondary = [],
                    B.sideNavigation.secondary = []
        }
        function z(a) {
            q.closeModal(),
                g.go(j.defaultStateName, {
                    userId: a
                }),
                w.sendEvent("searchViewProfile")
        }
        function A() {
            v.closePanel("main"),
                q.closeModal()
        }
        var B = this
            , C = {
                myStats: {
                    oasisId: "200285",
                    state: "playerstatistics",
                    icon: "common-stat"
                },
                news: {
                    oasisId: "201184",
                    href: "http://rainbow6.ubi.com/",
                    icon: "common-info"
                },
                rewards: {
                    oasisId: "207554",
                    state: "rewards({seasonId:" + i.rewardsSeason + "})",
                    icon: "stat-reward"
                },
                search: {
                    oasisId: "201069",
                    "class": ["search"],
                    click: function () {
                        var a = {
                            searchPlayer: B.searchPlayer,
                            viewProfile: B.viewProfile
                        }
                            , b = {
                                platform: B.platform
                            };
                        q.openModal({
                            rsMoleculeModal: "assets/tpl/components/search.player.modal.tpl.html",
                            rsPropCloseOnBackdropClick: !0,
                            rsPropModalTitle: 201069,
                            rsFnActions: a,
                            rsPropData: b
                        }),
                            w.sendEvent("headerSearch")
                    },
                    icon: "action-search"
                },
                watchlist: {
                    oasisId: "202290",
                    "class": ["watchlist"],
                    click: function () {
                        t.load(),
                            B.watchlist.isLoading = t.getIsLoading(),
                            v.togglePanel("main", "right"),
                            w.sendEvent("showWatchlist")
                    },
                    icon: "action-watchlist"
                },
                logout: {
                    oasisId: "57921",
                    click: function () {
                        A(),
                            B.logout()
                    },
                    icon: "action-logout"
                }
            };
        f.$on("offcanvas", function (a, b, c, d) {
            if ("main" === b && "right" === c)
                if (d)
                    C.watchlist["class"].push("active");
                else {
                    var e = C.watchlist["class"].indexOf("active");
                    C.watchlist["class"].splice(e, 1)
                }
        }),
            B.uplayIframeUrl = r.getUplayIframeUrl(),
            f.$on("$translateChangeSuccess", function (a, b) {
                B.uplayIframeUrl = r.getUplayIframeUrl()
            }),
            B.logged = !1,
            B.username = "",
            B.avatarUrl = "",
            B.appVersion = "v" + k,
            B.platformList = i.platforms,
            B.headerNavigation = {
                main: [C.myStats, C.news, C.rewards],
                secondary: []
            },
            B.sideNavigation = {
                main: [C.myStats, C.news, C.rewards],
                secondary: []
            },
            B.notificationList = s.list,
            B.watchlist = {},
            B.watchlist.watchedPlayers = t.list,
            B.watchlist.importFriends = t.importFriends,
            B.watchlist.isLoading = t.getIsLoading(),
            B.watchlist.maxPlayer = t.getMaxPlayer();
        f.$on("watchlist.changed", x);
        if (f.$on("$stateChangeSuccess", function (a, b, c, d, e) {
            y()
        }),
            y(),
            b.isUndefined(a.location.origin)) {
            var D = a.location.protocol
                , E = a.location.hostname
                , F = a.location.port ? ":" + a.location.port : "";
            a.location.origin = D + "//" + E + F
        }
        return b.extend(B, {
            messup: function () {
                h.config.headers.Authorization += "-666",
                    l.init(),
                    r.reset()
            },
            login: function () {
                console.log("login")
            },
            logout: function () {
                r.logout().then(function () {
                    s.reset(),
                        t.reset(),
                        u.reset(),
                        g.go("home", null, {
                            reload: !0
                        })
                }, function () {
                    g.go("home", null, {
                        reload: !0
                    })
                })
            },
            toggleNavigation: function () {
                v.togglePanel("main", "left")
            },
            navigateHome: function () {
                var a = l.parseSessionSync();
                b.isDefined(a) ? g.go("playerstatistics", {
                    userId: a.userId
                }) : g.go("home")
            },
            navigateProfile: function () {
                var a = l.parseSessionSync();
                g.go("playerstatistics", {
                    userId: a.userId
                })
            },
            changePlatform: function (a) {
                A(),
                    g.go(g.current.name, {
                        platform: a
                    })
            },
            viewProfile: z,
            searchPlayer: function (a) {
                return o.search(a)
            },
            addFriend: function (a) {
                return p.sendInvite({
                    friends: [a]
                })
            },
            removeFriend: function (a) {
                return p.clearRelationship({
                    friendPid: a
                })
            },
            localeChanged: function (a) {
                function c() {
                    var c = e.get("$stateParams")
                        , d = g.current.name
                        , f = b.copy(c);
                    f.locale = a,
                        g.go(d, f)
                }
                B.logged ? u.updateProperty("web.user.profile", "languageCode", a).then(c, c) : c()
            }
        })
    }
    e.$inject = ["$log", "$injector", "$scope", "$state", "httpService", "CONFIG", "INDEX", "VERSION", "sessionService", "localizationService", "platformsService", "searchService", "friendsApiService", "rsMoleculeModalService", "uplayConnectService", "notificationsService", "watchlistService", "entitiesService", "rsLayoutOffcanvasService", "googleAnalyticsService"],
        b.module("g2w.index").controller("indexController", e)
}(window, window.angular, window._);

/** module("g2w.index").directive("forceScrollTop", e) */
!function (a, b, c) {
    "use strict";
    function d(a, b) {
        a.$on("$stateChangeSuccess", function () {
            b[0].scrollTop = 0
        })
    }
    function e() {
        return {
            restrict: "A",
            controller: d
        }
    }
    d.$inject = ["$rootScope", "$element"],
        b.module("g2w.index").directive("forceScrollTop", e)
}(window, window.angular);

/** module("g2w.index").run(d) */
!function (a, b, c) {
    "use strict";
    function d(a, c, d, e, f, g, h, i) {
        e.setRedirectLogin(function () {
            f.closeModal(),
                h.sendClickEvent("Rainbow 6 G2W - Login");
            var a = g.getRejectedState();
            b.isDefined(a) ? c.go(a.name, a.params, {
                reload: !0
            }) : c.go(d.defaultStateName, {}, {
                reload: !0
            })
        }),
            a.$on("$stateChangeSuccess", function () {
                i.closePanel("main")
            })
    }
    d.$inject = ["$rootScope", "$state", "INDEX", "uplayConnectService", "rsMoleculeModalService", "permissionsService", "tagCommanderService", "rsLayoutOffcanvasService"],
        b.module("g2w.index").run(d)
}(window, window.angular);

/** module("g2w.resolve").constant("SESSION", d) */
!function (a, b, c) {
    "use strict";
    var d = {
        noData: "NO DATA"
    };
    b.module("g2w.resolve").constant("SESSION", d)
}(window, window.angular);

/** module("g2w.resolve").service("localizationService", d) */
!function (a, b, c) {
    "use strict";
    function d(a, c, d, e, f, g, h, i, j) {
        function k() {
            return c.info("[localization.service]", "parseLocalizationData"),
                l
        }
        var l, m, n;
        return {
            getUrlLocale: function () {
                var c, d = a.get("$translate"), e = a.get("$location"), f = e.$$path, g = f.match(/([a-z]{2}?([-][a-z]{2,}|))/gm);
                return b.isDefined(g) && null !== g && g.length >= 1 && (c = d.negotiateLocale(g[0])),
                    b.isUndefined(c) && (c = d.determinePreferredLanguage()),
                    c
            },
            initSession: function (a) {
                return c.info("[localization.service]", "initSession", a),
                    l = {},
                    l.session = a,
                    d.when(a)
            },
            init: function () {
                l = l || {};
                var a = l.session;
                m = e.next,
                    n = e.toParams;
                var f, g;
                b.isDefined(a) && b.isDefined(a.entity) && (g = a.entity.obj.languageCode);
                var h = n.locale.toLowerCase()
                    , i = j.determinePreferredLanguage();
                return c.info("[localization.service]", "init", "entityLocale", g, "stateLocale", h, "defaultLocale", i),
                    (b.isUndefined(h) || "" === h) && (f = j.negotiateLocale(g)),
                    b.isUndefined(f) && b.isDefined(h) && (f = j.negotiateLocale(h)),
                    b.isUndefined(f) && (f = j.negotiateLocale(i)),
                    c.info("[localization.service]", "init", "targetLocaleCode", f),
                    l.code = f,
                    d.when(l)
            },
            parseLocalizationSync: function () {
                return k()
            },
            parseLocalization: function () {
                return d.when(k())
            },
            redirect: function () {
                return d(function (a, b) {
                    n.locale !== l.code ? (n.locale = l.code,
                        c.info("[localization.service]", "*resolve*", "locale redirect"),
                        b(h.generateError("badRequest", "localization.service", "redirect", {
                            name: m.name,
                            params: n,
                            reload: !0
                        }))) : (c.info("[localization.service]", "*resolve*", "set localization"),
                            j.use(l.code).then(function () {
                                a(l)
                            }))
                })
            }
        }
    }
    d.$inject = ["$injector", "$log", "$q", "$state", "$stateParams", "$timeout", "errorsService", "CONFIG", "$translate"],
        b.module("g2w.resolve").service("localizationService", d)
}(window, window.angular);

/** module("g2w.resolve").service("platformsService", e) */
!function (a, b, c, d) {
    "use strict";
    function e(a, d, e, f, g, h, i, j, k, l, m) {
        function n(a) {
            var c = s[0];
            for (var d in a) {
                var e = a[d];
                b.isDefined(e.isLatestPlayed) && (c = d)
            }
            return c
        }
        function o() {
            a.info("[platform.service]", "parseData", p);
            var c = {};
            return b.isDefined(p) && b.isDefined(p.type) && (c.type = p.type),
                c
        }
        var p, q, r, s = i.platforms;
        return {
            getUrlPlatform: function () {
                var a, c = d.get("$location"), e = c.$$path, f = e.match(/(([a-z]{2}-[a-z]{2,})\/(psn|uplay|xbl))/);
                return b.isDefined(f) && null !== f && f.length >= 1 && (a = f[3]),
                    a
            },
            getPlatformProfile: function () {
                var c = p.session.profiles[p.type];
                return b.isUndefined(c) && (c = b.copy(p.session.profiles.uplay),
                    delete c.profileId),
                    a.info("[platforms.service]", "getPlatformProfile", c),
                    e.when(c)
            },
            initSession: function (b) {
                return a.info("[platforms.service]", "initSession", b),
                    p = {},
                    p.session = b,
                    e.when(b)
            },
            init: function () {
                return e(function (c, d) {
                    p = p || {},
                        q = f.next,
                        r = f.toParams;
                    var e = r.platform;
                    s.indexOf(e) !== -1 ? p.type = e : b.isDefined(p) && b.isDefined(p.session) && (p.type = n(p.session.profiles)),
                        a.info("[platform.service]", "init", p.type),
                        c(p)
                })
            },
            redirect: function () {
                return e(function (b, c) {
                    r.platform !== p.type ? (r.platform = p.type,
                        a.info("[platforms.service]", "*resolve*", "platform redirect"),
                        c(m.generateError("badRequest", "platforms.service", "redirect", {
                            name: q.name,
                            params: r,
                            reload: !0
                        }))) : b(p)
                })
            },
            parsePlatformSync: function () {
                return o()
            },
            parsePlatform: function () {
                return e.when(o())
            },
            processActions: function () {
                function a() {
                    var a = {
                        profileId: n.userId,
                        spaceId: m,
                        locale: h.use()
                    };
                    return k.getUplayProfileActions(a)
                }
                function d(a) {
                    p.uplayActions = l.actionList(a.actions);
                    var b = p.uplayActions;
                    g.actions = b;
                    var d = 0
                        , f = c.filter(b, {
                            isAvailable: !0,
                            isCompleted: !0,
                            isBadge: !1
                        })
                        , h = c.filter(b, {
                            isAvailable: !0,
                            isBadge: !1
                        });
                    return d = f.length / h.length * 100,
                        e.when(g)
                }
                function f() {
                    if (b.isUndefined(g.actions))
                        return e.when(g);
                    var a = c.find(g.actions, {
                        id: o
                    });
                    return b.isUndefined(a) ? e.when(g) : a.isAvailable && !a.isCompleted ? e(function (b, c) {
                        var d = {
                            profileId: p.session.userId,
                            id: o,
                            spaceId: m
                        };
                        k.postUplayProfileActions(d).then(function () {
                            a.isJustCompleted = !0,
                                b(g)
                        }, function () {
                            b(g)
                        })
                    }) : e.when(g)
                }
                var g = {
                    type: p.type
                }
                    , j = p.type
                    , m = i.spaceIds[j]
                    , n = p.session.profiles[j]
                    , o = "10";
                return b.isUndefined(n) ? e.when(g) : a().then(d).then(f)["catch"]()
            }
        }
    }
    e.$inject = ["$log", "$injector", "$q", "$state", "$timeout", "$translate", "CONFIG", "entitiesApiService", "uplaywinApiService", "uplaywinParserService", "errorsService"],
        b.module("g2w.resolve").service("platformsService", e)
}(window, window.angular, window._);

/** module("g2w.resolve").service("sessionService", e) */
!function (a, b, c, d) {
    "use strict";
    function e(a, d, e, f, g, h, i, j, k, l, m, n) {
        function o(c) {
            var d;
            return b.isDefined(c) && c.forEach(function (a) {
                if (b.isUndefined(d))
                    d = a;
                else {
                    var c = new Date(a.lastSessionDate)
                        , e = new Date(d.lastSessionDate)
                        , f = c > e;
                    f && (d = a)
                }
            }),
                a.info("[session.service]", "_latestApplication", d),
                d
        }
        function p() {
            var d, e = o(q.applications, q.profiles), g = {};
            c.forEach(q.profiles, function (a, c) {
                r.indexOf(c) !== -1 && (g[c] = b.copy(a),
                    b.isDefined(e) && g[c].profileId === e.profileId && (g[c].isLatestPlayed = !0,
                        d = c))
            }),
                d || (d = f.noData);
            var h = {};
            return b.isDefined(q) && b.isDefined(q.session) && (h = {
                userId: q.session.user_id,
                id: q.session.sessionId,
                ticket: q.session.ticket,
                profiles: g,
                userType: "player",
                entity: q.entitiy,
                autoPlatform: d
            }),
                a.info("[session.service]", "parseSessionData", h),
                h
        }
        var q, r = e.platforms;
        return {
            init: function () {
                return q = {},
                    h.init(e.appIds.g2w)
            },
            getTicket: function () {
                return h.getTicket().then(function (a) {
                    q.session = a
                })
            },
            getProfiles: function () {
                return k.getProfiles({
                    userId: q.session.user_id
                }, [m.byPlatform]).then(function (b) {
                    return a.info("[session.service]", "getProfiles", b),
                        q.profiles = b,
                        q
                }, function (b) {
                    return a.warn("[session.service]", "getProfiles", b),
                        d.reject(g.generateError("internal", "index.session.service", "getProfiles"))
                })
            },
            getEntity: function () {
                return a.info("[session.service]", "getEntities"),
                    n.init(q.session.user_id).then(function (b) {
                        a.info("[session.service]", "getEntities done", b),
                            q.entity = n.getProperty("web.user.profile")
                    })
            },
            getApplications: function () {
                var d, f = [], g = c.mapValues(q.profiles, "profileId"), h = [];
                for (d in e.appIds)
                    r.indexOf(d) !== -1 && (h.push(e.appIds[d]),
                        b.isDefined(g[d]) && f.push(g[d]));
                return l.getProfileApplicationsByProfileList({
                    profileIds: f.join(","),
                    applicationIds: h.join(",")
                }).then(function (b) {
                    a.info("[session.service]", "getApplications", b),
                        q.applications = b.applications
                }, function (b) {
                    return a.warn("[session.service]", "getApplications", b),
                        {}
                })
            },
            parseSessionSync: function () {
                return p()
            },
            parseSession: function () {
                return d.when(p())
            }
        }
    }
    e.$inject = ["$log", "$q", "CONFIG", "SESSION", "errorsService", "uplayConnectService", "parsersService", "profileApiService", "usersApiService", "applicationusedApiService", "profileParserService", "entitiesService"],
        b.module("g2w.resolve").service("sessionService", e)
}(window, window.angular, window._);

/** module("g2w.section.errors").config(h) */
!function (a, b, c, d) {
    "use strict";
    function e(a, b, c, d, e, f) {
        return a(function (a, c) {
            var d = e.toParams.code;
            switch (d) {
                case "404":
                    a();
                    break;
                case "500":
                    a();
                    break;
                default:
                    c(b.generateError("notFound", "errors.config", "resolve", {
                        params: e.toParams
                    }))
            }
        })
    }
    function f(a, b, c, d, f, g) {
        return e(a, b, c, d, f, g)
    }
    function g(a, b, c, d, f, g, h) {
        return e(a, b, c, d, f, g)
    }
    function h(a, b) {
        a.otherwise(function (a) {
            var b = a.get("$state")
                , c = (a.get("$stateParams"),
                    a.get("platformsService"))
                , d = a.get("localizationService")
                , e = b.toParams || {};
            e.locale = e.locale || d.getUrlLocale(),
                e.platform = e.platform || c.getUrlPlatform(),
                e.code = 404,
                b.go("errorsLogged", e)
        }),
            b.state("errors", {
                parent: "locale",
                url: "/errors/:code",
                data: {
                    permissions: {
                        only: ["anonymous"],
                        redirection: {
                            player: {
                                name: "errorsLogged"
                            }
                        }
                    }
                },
                views: {
                    "section@root": {
                        templateUrl: "assets/tpl/errors.tpl.html",
                        controller: "errorController",
                        controllerAs: "vm"
                    }
                },
                resolve: {
                    isAllowed: ["session", "permissionsService", function (a, b) {
                        return b.isUserAllowed()
                    }
                    ],
                    error: f
                }
            }).state("errorsLogged", {
                parent: "platform",
                url: "/errors/:code",
                data: {
                    permissions: {
                        only: ["player"],
                        redirection: {
                            anonymous: {
                                name: "errors"
                            }
                        }
                    }
                },
                views: {
                    "section@root": {
                        templateUrl: "assets/tpl/errors.tpl.html",
                        controller: "errorController",
                        controllerAs: "vm"
                    }
                },
                resolve: {
                    error: g
                }
            })
    }
    f.$inject = ["$q", "errorsService", "session", "locale", "$state", "isAllowed"],
        g.$inject = ["$q", "errorsService", "session", "locale", "$state", "isAllowed", "tagCommander"],
        h.$inject = ["$urlRouterProvider", "$stateProvider"],
        b.module("g2w.section.errors").config(h)
}(window, window.angular, window._);

/** module("g2w.section.errors").controller("errorController", d) */
!function (a, b, c) {
    "use strict";
    function d(a, c, d, e) {
        function f(a, c) {
            var d = "playerstatistics";
            b.isDefined(a) && (d = 'playerstatistics({userId:"' + a.userId + '"})');
            var e = [{
                oasisId: "193807",
                uiSref: d,
                hasSession: !0
            }, {
                oasisId: "193808",
                href: "http://forums.ubi.com/forumdisplay.php/717-Rainbow-Six-Siege",
                hasSession: !1
            }]
                , f = [];
            return _.forEach(e, function (b) {
                (!b.hasSession || a && "player" === a.userType) && f.push(b)
            }),
                f
        }
        var g = this
            , h = c.code;
        switch (h) {
            case "404":
                g.code = 404,
                    g.title = "193804",
                    g.message = "175764",
                    g.links = f(d, e);
                break;
            case "500":
                g.code = 500,
                    g.title = "194970",
                    g.message = "194944",
                    g.links = f(d, e)
        }
    }
    d.$inject = ["$log", "$stateParams", "session", "locale"],
        b.module("g2w.section.errors").controller("errorController", d)
}(window, window.angular);

/** module("g2w.section.errors").config(e) */
!function (a, b, c, d) {
    "use strict";
    function e(a, b) {
        a.decorator("errorsService", ["$delegate", function (a) {
            var b = a.generateError;
            return a.generateError = function (a, c, d, e) {
                var f = b(a, c, d, e);
                return "internal" !== a && "notFound" !== a || ("internal" === a && (f.code = 500),
                    "notFound" === a && (f.code = 404),
                    f.redirection = f.redirection || {},
                    "anonymous" === f.userType ? (f.redirection.name = "errors",
                        f.redirection.params = f.redirection.params || {},
                        f.redirection.params.code = f.code) : (f.redirection.name = "errorsLogged",
                            f.redirection.params = f.redirection.params || {},
                            f.redirection.params.code = f.code)),
                    f
            }
                ,
                a
        }
        ])
    }
    e.$inject = ["$provide", "$injector"],
        b.module("g2w.section.errors").config(e)
}(window, window.angular, window._);

/** module("g2w.section.errors").factory("errorInterceptor", d).config(e) */
!function (a, b, c) {
    "use strict";
    function d(a, c, d) {
        var e = {
            responseError: function (c) {
                return b.isDefined(c.data) && [401, 403].indexOf(c.data.httpCode) >= 0 && (d.get("uplayConnectService").reset(),
                    d.get("sessionService").init(),
                    d.get("permissionsService").setUserType("anonymous"),
                    d.get("notificationsService").reset(),
                    d.get("watchlistService").reset(),
                    d.get("entitiesService").reset(),
                    c.redirection = {
                        name: d.get("$state").next.name,
                        params: d.get("$state").toParams,
                        reload: !0
                    },
                    d.get("errorsService").handleError(c)),
                    a.reject(c)
            }
        };
        return e
    }
    function e(a) {
        a.interceptors.push("errorInterceptor")
    }
    d.$inject = ["$q", "$log", "$injector"],
        e.$inject = ["$httpProvider"],
        b.module("g2w.section.errors").factory("errorInterceptor", d).config(e)
}(window, window.angular);

/** module("g2w.section.home").config(d) */
!function (a, b, c) {
    "use strict";
    function d(a, b, c, d) {
        b.when("/:locale", ["$injector", function (a) {
            var b, c = a.get("$state"), d = a.get("localizationService");
            return b = c.next && "home" !== c.next.name ? c.href(c.next, c.toParams) : "/" + d.getUrlLocale() + "/home"
        }
        ]),
            a.state("home", {
                parent: "locale",
                url: "/home",
                views: {
                    "section@root": {
                        templateUrl: "assets/tpl/home.tpl.html"
                    }
                },
                data: {
                    permissions: {
                        only: ["anonymous"],
                        redirection: {
                            player: {
                                name: d.defaultStateName
                            }
                        }
                    },
                    tagCommander: {
                        pageName: "G2W Homepage",
                        sectionName: "Home pages"
                    }
                },
                resolve: {
                    isAllowed: ["session", "permissionsService", function (a, b) {
                        return b.isUserAllowed()
                    }
                    ]
                }
            })
    }
    d.$inject = ["$stateProvider", "$urlRouterProvider", "$translateProvider", "INDEX"],
        b.module("g2w.section.home").config(d)
}(window, window.angular);

/** module("g2w.section.playerstatistics").config(k) */
!function (a, b, c, d) {
    "use strict";
    function e(a, c, d, e, f, g) {
        return f(function (c, f) {
            var h, i, j = b.copy(e), k = b.copy(d.toParams);
            b.isDefined(k.userId) ? (h = k.userId,
                i = k) : b.isDefined(j.userId) && (h = j.userId,
                    i = j),
                b.isUndefined(h) || "" === h ? (i.userId = a.userId,
                    f(g.generateError("badRequest", "playerstatistics.service", "userIdResolve", {
                        name: "playerstatistics.multiplayer",
                        params: i,
                        reload: !0
                    }))) : c(h)
        })
    }
    function f(a, c, e, f, g, h, i, j, k, l) {
        if (j.info("[playerstatistics.config]", "playerStatisticsProfileResolve"),
            a.userId === c) {
            j.info("[playerstatistics.config]", "playerStatisticsProfileResolve", f);
            var m = b.copy(f);
            return m.isMyProfile = !0,
                m.platformType = e.type,
                k.when(m)
        }
        return k(function (a, f) {
            g.search({
                userId: c
            }, [h.search]).then(function (c) {
                var f = c && c.length > 0 ? c[0][e.type] : d;
                j.info("[playerstatistics.config]", "playerStatisticsProfileResolve", f),
                    b.isUndefined(f) && (f = b.copy(c[0].uplay),
                        delete f.profileId),
                    a(f)
            }, function (a) {
                a.handled || f(l.generateError("notFound", "playerstatistics.service", "playerPlatformProfile"))
            })
        })
    }
    function g(a, b, c, d, e, f, g, h) {
        return g.getUsersStatistics([d]).then(function (a) {
            d.profileId === c.profileId && h.setCustomDimension("userClearance", d.clearance)
        })
    }
    function h(a, b, c, d) {
        return d.info("[playerstatistics.config]", "statLoaderResolve"),
            b.loadDefinitions().then(function () {
                c.init()
            })
    }
    function i(a, c, d, e, f, g) {
        return f.info("[playerstatistics.config]", "multiplayerStatisticsResolve"),
            b.isUndefined(c) || b.isUndefined(c.profileId) ? g.when({
                isNoData: !0
            }) : g(function (a, b) {
                d.getStatisticsSections("multiplayer", ["main", "secondary", "mostUsedOperators", "operators", "weapons", "modes"], c.profileId).then(function (b) {
                    a(b)
                }, function (b) {
                    a({
                        isNoData: !0
                    })
                })
            })
    }
    function j(a, c, d, e, f, g) {
        return f.info("[playerstatistics.config]", "terrohuntStatisticsResolve"),
            b.isUndefined(c) || b.isUndefined(c.profileId) ? g.when({
                isNoData: !0
            }) : g(function (a, b) {
                d.getStatisticsSections("terrohunt", ["main", "secondary", "mostUsedOperators", "operators", "weapons", "modes"], c.profileId).then(function (b) {
                    a(b)
                }, function (b) {
                    a({
                        isNoData: !0
                    })
                })
            })
    }
    function k(a, b) {
        b.when("/:locale/:platform/player-statistics", "/:locale/:platform/player-statistics/:userId/multiplayer"),
            b.when("/:locale/:platform/player-statistics/:userId", "/:locale/:platform/player-statistics/:userId/multiplayer"),
            a.state("playerstatistics", {
                parent: "platform",
                url: "/player-statistics/:userId",
                views: {
                    "section@root": {
                        templateUrl: "assets/tpl/playerstatistics.tpl.html"
                    }
                },
                data: {
                    permissions: {
                        only: ["player"],
                        redirection: {
                            anonymous: {
                                name: "home"
                            }
                        }
                    }
                },
                resolve: {
                    userId: e,
                    playerPlatformProfile: f,
                    playerProgression: g,
                    statisticDefinitions: h
                }
            }).state("playerstatistics.multiplayer", {
                url: "/multiplayer",
                views: {
                    playerstatisticsView: {
                        templateUrl: "assets/tpl/playerstatistics.tab.tpl.html",
                        controller: "playerstatisticsController",
                        controllerAs: "vm"
                    }
                },
                data: {
                    permissions: {
                        only: ["player"],
                        redirection: {
                            anonymous: {
                                name: "home"
                            }
                        }
                    },
                    tagCommander: {
                        pageName: "Overview",
                        sectionName: "Multiplayer"
                    }
                },
                resolve: {
                    statistics: i
                }
            }).state("playerstatistics.terrohunt", {
                url: "/terrohunt",
                views: {
                    playerstatisticsView: {
                        templateUrl: "assets/tpl/playerstatistics.tab.tpl.html",
                        controller: "playerstatisticsController",
                        controllerAs: "vm"
                    }
                },
                data: {
                    permissions: {
                        only: ["player"],
                        redirection: {
                            anonymous: {
                                name: "home"
                            }
                        }
                    },
                    tagCommander: {
                        pageName: "Overview",
                        sectionName: "Terrorists Hunt"
                    }
                },
                resolve: {
                    statistics: j
                }
            })
    }
    e.$inject = ["session", "tagCommander", "$state", "$stateParams", "$q", "errorsService"],
        f.$inject = ["session", "userId", "platform", "sessionPlatformProfile", "profileApiService", "profileParserService", "CONFIG", "$log", "$q", "errorsService"],
        g.$inject = ["watchlist", "rdvServicesConfig", "sessionPlatformProfile", "playerPlatformProfile", "$log", "$q", "statisticsService", "googleAnalyticsService"],
        h.$inject = ["userId", "statisticsService", "playerstatisticsService", "$log"],
        i.$inject = ["statisticDefinitions", "playerPlatformProfile", "playerstatisticsService", "errorsService", "$log", "$q"],
        j.$inject = ["statisticDefinitions", "playerPlatformProfile", "playerstatisticsService", "errorsService", "$log", "$q"],
        k.$inject = ["$stateProvider", "$urlRouterProvider"],
        b.module("g2w.section.playerstatistics").config(k)
}(window, window.angular, window._);

/** module("g2w.section.playerstatistics").constant("PLAYERSTATISTICS", d) */
!function (a, b, c) {
    "use strict";
    var d = {
        staticSchemas: {
            main: {
                multiplayer: {
                    time: {
                        statId: "generalpvp_timeplayed"
                    },
                    matches: {
                        statId: "generalpvp_matchplayed"
                    },
                    win: {
                        icon: "stat-match",
                        main: {
                            statId: "generalpvp_matchwon"
                        },
                        second: {
                            statId: "generalpvp_matchlost"
                        }
                    },
                    kill: {
                        icon: "stat-killdeath",
                        main: {
                            statId: "generalpvp_kills"
                        },
                        second: {
                            statId: "generalpvp_death"
                        }
                    },
                    accuracy: {
                        icon: "stat-accuracy",
                        main: {
                            statId: "generalpvp_bullethit"
                        },
                        second: {
                            statId: "generalpvp_bulletfired"
                        }
                    },
                    performance: {
                        label: "201678",
                        data: [{
                            label: "191216",
                            statId: "generalpvp_killassists"
                        }, {
                            label: "191217",
                            statId: "generalpvp_revive"
                        }, {
                            label: "191210",
                            statId: "generalpvp_headshot"
                        }, {
                            label: "191367",
                            statId: "generalpvp_penetrationkills"
                        }, {
                            label: "191203",
                            statId: "generalpvp_meleekills"
                        }]
                    }
                },
                terrohunt: {
                    time: {
                        statId: "generalpve_timeplayed"
                    },
                    matches: {
                        statId: "generalpve_matchplayed"
                    },
                    win: {
                        icon: "stat-match",
                        main: {
                            statId: "generalpve_matchwon"
                        },
                        second: {
                            statId: "generalpve_matchlost"
                        }
                    },
                    kill: {
                        icon: "stat-killdeath",
                        main: {
                            statId: "generalpve_kills"
                        },
                        second: {
                            statId: "generalpve_death"
                        }
                    },
                    accuracy: {
                        icon: "stat-accuracy",
                        main: {
                            statId: "generalpve_bullethit"
                        },
                        second: {
                            statId: "generalpve_bulletfired"
                        }
                    },
                    performance: {
                        label: "201678",
                        data: [{
                            label: "191216",
                            statId: "generalpve_killassists"
                        }, {
                            label: "191217",
                            statId: "generalpve_revive"
                        }, {
                            label: "191210",
                            statId: "generalpve_headshot"
                        }, {
                            label: "191367",
                            statId: "generalpve_penetrationkills"
                        }, {
                            label: "191203",
                            statId: "generalpve_meleekills"
                        }]
                    }
                }
            },
            secondary: {
                multiplayer: {
                    defense: [{
                        statId: "rescuehostagepvp_bestscore",
                        icon: "stat-win"
                    }, {
                        statId: "plantbombpvp_bestscore",
                        icon: "stat-win"
                    }, {
                        statId: "secureareapvp_bestscore",
                        icon: "stat-win"
                    }],
                    casualStats: {
                        label: "192624",
                        winLoss: {
                            matchesWon: {
                                statId: "casualpvp_matchwon"
                            },
                            matchesLoss: {
                                statId: "casualpvp_matchlost"
                            },
                            icon: "stat-match"
                        },
                        data: [{
                            statId: "casualpvp_timeplayed"
                        }, {
                            statId: "casualpvp_matchwon"
                        }, {
                            statId: "casualpvp_matchlost"
                        }, {
                            statId: "casualpvp_matchplayed"
                        }, {
                            statId: "casualpvp_kdratio"
                        }, {
                            statId: "casualpvp_kills"
                        }, {
                            statId: "casualpvp_death"
                        }]
                    },
                    rankedStats: {
                        label: "192623",
                        winLoss: {
                            matchesWon: {
                                statId: "rankedpvp_matchwon"
                            },
                            matchesLoss: {
                                statId: "rankedpvp_matchlost"
                            },
                            icon: "stat-match"
                        },
                        data: [{
                            statId: "rankedpvp_timeplayed"
                        }, {
                            statId: "rankedpvp_matchwon"
                        }, {
                            statId: "rankedpvp_matchlost"
                        }, {
                            statId: "rankedpvp_matchplayed"
                        }, {
                            statId: "rankedpvp_kdratio"
                        }, {
                            statId: "rankedpvp_kills"
                        }, {
                            statId: "rankedpvp_death"
                        }]
                    },
                    performance: {
                        label: "201678",
                        data: [{
                            statId: "generalpvp_killassists"
                        }, {
                            statId: "generalpvp_revive"
                        }, {
                            statId: "generalpvp_headshot"
                        }, {
                            statId: "generalpvp_penetrationkills"
                        }, {
                            statId: "generalpvp_meleekills"
                        }]
                    }
                },
                terrohunt: {
                    casualStats: {
                        label: "192630",
                        data: [{
                            statId: "allterrohuntcoop_normal_bestscore"
                        }, {
                            statId: "allterrohuntcoop_hard_bestscore"
                        }, {
                            statId: "allterrohuntcoop_realistic_bestscore"
                        }]
                    },
                    rankedStats: {
                        label: "192637",
                        data: [{
                            statId: "allterrohuntsolo_normal_bestscore"
                        }, {
                            statId: "allterrohuntsolo_hard_bestscore"
                        }, {
                            statId: "allterrohuntsolo_realistic_bestscore"
                        }]
                    },
                    performance: {
                        label: "201678",
                        data: [{
                            statId: "generalpve_killassists"
                        }, {
                            statId: "generalpve_revive"
                        }, {
                            statId: "generalpve_headshot"
                        }, {
                            statId: "generalpve_penetrationkills"
                        }, {
                            statId: "generalpve_meleekills"
                        }]
                    }
                }
            }
        }
    };
    b.module("g2w.section.playerstatistics").constant("PLAYERSTATISTICS", d)
}(window, window.angular);

/** module("g2w.section.playerstatistics").controller("playerstatisticsController", d) */
!function (a, b, c) {
    "use strict";
    function d(a, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s) {
        function t() {
            var a = m.isWatched(v.userId);
            c.debug("[playerstatistics.controller] watchlist.service", "update", v.userId, a),
                u.playerIdentity.isWatched = a
        }
        var u = this
            , v = b.copy(k);
        u.isSessionProfile = j.profileId === v.profileId,
            u.playerIdentity = {
                username: v.username,
                image: v.avatar.smallUrl,
                platform: v.platformType,
                clearance: v.clearance,
                ranks: v.ranks,
                compare: function () {
                    p.show([j, v]),
                        q.sendEvent("profileWatched", u.playerIdentity.isWatched)
                },
                isWatched: m.isWatched(v.userId),
                toggleWatched: function () {
                    c.debug("[playerstatistics.controller] watchlist.service", "toggleWatched", v.userId),
                        m.isWatched(v.userId) ? m.remove(v.userId).then(t, t) : m.add(v.userId).then(t, t)
                },
                hideCompare: u.isSessionProfile,
                hideWatchlist: u.isSessionProfile,
                navigateToRewards: function () {
                    d.go("rewardsLogged", {
                        seasonId: g.rewardsSeason
                    })
                }
            };
        var w = a.$on("watchlist.changed", t);
        a.$on("$destroy", function () {
            w()
        }),
            t(),
            u.isNoData = b.isUndefined(v) || b.isUndefined(n) || n.isNoData || b.isUndefined(n.main.time.value) || 0 === n.main.time.value,
            u.statistics = n,
            u.navigation = [{
                uiSref: "playerstatistics.multiplayer",
                label: "200286"
            }, {
                uiSref: "playerstatistics.terrohunt",
                label: "200287"
            }],
            u.isMyProfile = v.isMyProfile,
            a.$on("rs.organism.tabs", function (a, c, f) {
                if ("player-statistics-navigation" === c) {
                    q.sendEvent("playerStatisticsNavigation", f);
                    var g, h, i, j, k, l, m = e.locale.split("-");
                    g = d.current.data.tagCommander.sectionName,
                        h = r.statisticsTabs[f],
                        i = m[0],
                        j = m[1].toUpperCase(),
                        k = e.platform,
                        b.isUndefined(u.playerIdentity.ranks) && b.isUndefined(u.playerIdentity.ranks.current) && (l = u.playerIdentity.ranks.current.icon),
                        s.sendPageEvent(h, g, i, j, k, l)
                }
            }),
            u.platformList = g.platforms,
            u.currentPlatform = i.type
    }
    d.$inject = ["$scope", "$log", "$state", "$stateParams", "rsMoleculeModalService", "CONFIG", "session", "platform", "sessionPlatformProfile", "playerPlatformProfile", "entitiesService", "watchlistService", "statistics", "uplayConnectService", "statisticsCompareService", "googleAnalyticsService", "TAG_COMMANDER", "tagCommanderService"],
        b.module("g2w.section.playerstatistics").controller("playerstatisticsController", d)
}(window, window.angular);

/** module("g2w.section.playerstatistics").service("playerstatisticsService", d) */
!function (a, b, c) {
    "use strict";
    function d(a, c, d, e, f, g, h, i, j, k) {
        function l(b) {
            function c(a) {
                return e.when(h.merge(a, this.schema))
            }
            var d;
            switch (b) {
                case "multiplayer":
                    d = "pvp";
                    break;
                case "terrohunt":
                    d = "pve"
            }
            var f = a.staticSchemas.main
                , g = f[b];
            return {
                statisticsIds: h.extractStatisticsIds(g),
                schema: g,
                processResult: c
            }
        }
        function m(b) {
            function c(a) {
                var c = h.merge(a, this.schema);
                return "multiplayer" === b && (c.casualStats.winLoss.matchesLoss = c.casualStats.winLoss.matchesLoss.value,
                    c.casualStats.winLoss.matchesWon = c.casualStats.winLoss.matchesWon.value,
                    c.rankedStats.winLoss.matchesLoss = c.rankedStats.winLoss.matchesLoss.value,
                    c.rankedStats.winLoss.matchesWon = c.rankedStats.winLoss.matchesWon.value),
                    e.when(c)
            }
            var d;
            switch (b) {
                case "multiplayer":
                    d = "pvp";
                    break;
                case "terrohunt":
                    d = "pve"
            }
            var f = a.staticSchemas.secondary
                , g = f[b];
            return {
                statisticsIds: h.extractStatisticsIds(g),
                schema: g,
                processResult: c
            }
        }
        function n(a) {
            function c(a) {
                for (var b = h.merge(a, this.querySchema), c = _.clone(this.schema), d = this.queryMode, f = !1, g = !1, i = 0; i < b.mostUsedOperators.values.length; i++) {
                    var j, k, l = b.mostUsedOperators.values[i], m = l.target, n = m.fullIndex, o = c.attack, p = c.defense, q = "operator" + d + "_kdratio:" + n, r = "operator" + d + "_roundwlratio:" + n, s = _.find(b.operatorsStats, {
                        statId: q
                    }), t = _.find(b.operatorsStats, {
                        statId: r
                    });
                    if (f || "atk" !== m.category || (f = !0,
                        o.isMostUsed = !g,
                        o.hasNodata = !1,
                        o.id = m.id,
                        s = _.find(b.operatorsStats, {
                            statId: q
                        }),
                        t = _.find(b.operatorsStats, {
                            statId: r
                        }),
                        h.isEmptyStat(s) || (o.playerKillDeath.value = s.value),
                        h.isEmptyStat(t) || (o.playerMatch.value = t.value),
                        o.playerTime.value = l.value,
                        j = _.find(m.heroStats, function (a) {
                            return a.indexOf(d) >= 0
                        }),
                        j += ":" + n,
                        k = _.find(b.operatorsStats, {
                            statId: j
                        }),
                        o.uniqueStat = k),
                        g || "def" !== m.category || (g = !0,
                            p.isMostUsed = !f,
                            p.hasNodata = !1,
                            p.id = m.id,
                            s = _.find(b.operatorsStats, {
                                statId: q
                            }),
                            t = _.find(b.operatorsStats, {
                                statId: r
                            }),
                            h.isEmptyStat(s) || (p.playerKillDeath.value = s.value),
                            h.isEmptyStat(t) || (p.playerMatch.value = t.value),
                            p.playerTime.value = l.value,
                            j = _.find(m.heroStats, function (a) {
                                return a.indexOf(d) >= 0
                            }),
                            j += ":" + n,
                            k = _.find(b.operatorsStats, {
                                statId: j
                            }),
                            p.uniqueStat = k),
                        f && g)
                        break
                }
                return _.forEach(b.mostUsedOperators.values, function (a) { }),
                    e.when(c)
            }
            var d;
            switch (a) {
                case "multiplayer":
                    d = "pvp";
                    break;
                case "terrohunt":
                    d = "pve"
            }
            var g = {
                attack: {
                    label: "200288",
                    role: "attacker",
                    id: "",
                    playerTime: {
                        label: "191229",
                        value: 0,
                        icon: "stat-time",
                        type: "time"
                    },
                    playerMatch: {
                        label: "190607",
                        value: 0,
                        icon: "stat-match",
                        type: "ratio"
                    },
                    playerKillDeath: {
                        label: "191200",
                        value: 0,
                        icon: "stat-killdeath",
                        type: "ratio"
                    },
                    hasNodata: !0,
                    isMostUsed: !1
                },
                defense: {
                    label: "200289",
                    role: "defender",
                    id: "",
                    playerTime: {
                        label: "191229",
                        value: 0,
                        icon: "stat-time",
                        type: "time"
                    },
                    playerMatch: {
                        label: "190607",
                        value: 0,
                        icon: "stat-match",
                        type: "ratio"
                    },
                    playerKillDeath: {
                        label: "191200",
                        value: 0,
                        icon: "stat-killdeath",
                        type: "ratio"
                    },
                    hasNodata: !0,
                    isMostUsed: !1
                }
            }
                , i = {
                    mostUsedOperators: {
                        statId: "operator" + d + "_mostused"
                    },
                    operatorsStats: {}
                }
                , j = f.getModel();
            return _.forEach(j.operators, function (a) {
                var c, e = "operator" + d + "_kdratio:" + a.fullIndex, f = "operator" + d + "_roundwlratio:" + a.fullIndex;
                _.forEach(a.heroStats, function (e) {
                    b.isUndefined(c) && e.indexOf(d) >= 0 && (c = {
                        statId: e + ":" + a.fullIndex
                    })
                }),
                    i.operatorsStats[c.statId] = {
                        statId: c.statId
                    },
                    i.operatorsStats[e] = {
                        statId: e
                    },
                    i.operatorsStats[f] = {
                        statId: f
                    }
            }),
                {
                    queryMode: d,
                    querySchema: i,
                    statisticsIds: h.extractStatisticsIds(i),
                    schema: g,
                    processResult: c
                }
        }
        function o(a) {
            function c(a) {
                var b = f.getModel()
                    , c = h.merge(a, this.schema);
                return _.forEach(c, function (a) {
                    _.forEach(a.operatorsList, function (a) {
                        var c = _.find(b.operators, {
                            id: a.id
                        });
                        _.find(b.ctus, {
                            index: c.ctuIndex
                        });
                        a.noData = h.isEmptyStat(a.playerTime),
                            a.noData && (delete a.playerTime.value,
                                delete a.playerMatch.value,
                                delete a.playerKillDeath.value,
                                delete a.uniqueStat.value)
                    })
                }),
                    e.when(c)
            }
            var d;
            switch (a) {
                case "multiplayer":
                    d = "pvp";
                    break;
                case "terrohunt":
                    d = "pve"
            }
            var g = {
                operatorsStats: {}
            }
                , i = {
                    filtersLabel: {
                        playerTime: "191229",
                        playerMatch: "190607",
                        playerKillDeath: "191200"
                    },
                    attacker: {
                        operatorsList: []
                    },
                    defender: {
                        operatorsList: []
                    }
                }
                , j = f.getModel();
            return _.forEach(j.ctus, function (a) {
                _.forEach(a.operators, function (a) {
                    var c = j.operators[a];
                    if (b.isDefined(c)) {
                        var e, f = "operator" + d + "_timeplayed:" + c.fullIndex, g = "operator" + d + "_roundwlratio:" + c.fullIndex, h = "operator" + d + "_kdratio:" + c.fullIndex;
                        _.forEach(c.heroStats, function (a) {
                            b.isUndefined(e) && a.indexOf(d) >= 0 && (e = {
                                statId: a + ":" + c.fullIndex
                            })
                        });
                        var k = {
                            id: c.id,
                            playerTime: {
                                value: 0,
                                icon: "stat-time",
                                type: "time",
                                statId: f
                            },
                            playerMatch: {
                                value: 0,
                                icon: "stat-match",
                                type: "ratio",
                                statId: g
                            },
                            playerKillDeath: {
                                value: 0,
                                icon: "stat-killdeath",
                                type: "ratio",
                                statId: h
                            },
                            uniqueStat: e
                        };
                        "atk" === c.category && i.attacker.operatorsList.push(k),
                            "def" === c.category && i.defender.operatorsList.push(k)
                    }
                })
            }),
                {
                    querySchema: g,
                    statisticsIds: h.extractStatisticsIds(i),
                    schema: i,
                    processResult: c
                }
        }
        function p(a) {
            function c(a) {
                var c = (f.getModel(),
                    h.merge(a, this.schema));
                _.forEach(c, function (a) {
                    var c = ["kills", "headshots", "bulletsFired", "hits", "accuracy"];
                    _.forEach(c, function (c) {
                        b.isUndefined(a.noData) ? a.noData = h.isEmptyStat(a[c]) : a.noData = a.noData && h.isEmptyStat(a[c])
                    }),
                        a.noData && _.forEach(c, function (b) {
                            delete a[b].value
                        })
                });
                var d = {
                    title: "193829",
                    filters: [{
                        id: "kills",
                        icon: "stat-kills",
                        label: "191205"
                    }, {
                        id: "headshots",
                        icon: "stat-headshot",
                        label: "191201"
                    }, {
                        id: "bulletsFired",
                        icon: "common-magazine",
                        label: "191206"
                    }, {
                        id: "hits",
                        icon: "stat-hits",
                        label: "191207"
                    }, {
                        id: "accuracy",
                        icon: "stat-accuracy",
                        label: "191202"
                    }],
                    weaponsList: c
                };
                return e.when(d)
            }
            var d, g = [];
            switch (a) {
                case "multiplayer":
                    d = "pvp";
                    break;
                case "terrohunt":
                    d = "pve"
            }
            var i = f.getModel();
            return _.forEach(i.shootingweapontypes, function (a) {
                if (b.isDefined(a.icon)) {
                    var c = {
                        id: a.id,
                        index: a.index,
                        icon: a.icon,
                        kills: {
                            statId: "weapontype" + d + "_kills:" + a.index,
                            value: 0
                        },
                        headshots: {
                            statId: "weapontype" + d + "_headshot:" + a.index,
                            value: 0
                        },
                        bulletsFired: {
                            statId: "weapontype" + d + "_bulletfired:" + a.index,
                            value: 0
                        },
                        hits: {
                            statId: "weapontype" + d + "_bullethit:" + a.index,
                            value: 0
                        },
                        accuracy: {
                            statId: "weapontype" + d + "_accuracy:" + a.index,
                            type: "ratio",
                            value: 0
                        }
                    };
                    g.push(c)
                }
            }),
                {
                    statisticsIds: h.extractStatisticsIds(g),
                    schema: g,
                    processResult: c
                }
        }
        function q(a) {
            function b(a) {
                var b = (f.getModel(),
                    h.merge(a, this.schema))
                    , c = {
                        title: "205577",
                        filters: [{
                            id: "kills",
                            icon: "stat-challenge",
                            label: "192635"
                        }, {
                            id: "headshots",
                            icon: "stat-lost",
                            label: "191360"
                        }, {
                            id: "bulletsFired",
                            icon: "stat-match",
                            label: "194826"
                        }, {
                            id: "hits",
                            icon: "stat-plays",
                            label: "191358"
                        }, {
                            id: "accuracy",
                            icon: "stat-win",
                            label: "199912"
                        }],
                        weaponsList: b
                    };
                return e.when(c)
            }
            var c, d = [];
            switch (a) {
                case "multiplayer":
                    c = "pvp";
                    break;
                case "terrohunt":
                    c = "pve"
            }
            var g = (f.getModel(),
                {
                    multiplayer: [{
                        id: "secureareapvp",
                        oasidId: "205574"
                    }, {
                        id: "rescuehostagepvp",
                        oasidId: "205575"
                    }, {
                        id: "plantbombpvp",
                        oasidId: "205576"
                    }],
                    terrohunt: [{
                        id: "plantbombpve",
                        oasidId: "192618"
                    }, {
                        id: "rescuehostagepve",
                        oasidId: "192619"
                    }, {
                        id: "protecthostagepve",
                        oasidId: "192620"
                    }, {
                        id: "terrohuntclassicpve",
                        oasidId: "192621"
                    }]
                });
            return _.forEach(g[a], function (a) {
                var b = {
                    icon: "placeholder",
                    title: a.oasidId,
                    kills: {
                        statId: a.id + "_matchwon",
                        label: "192635",
                        value: 0
                    },
                    headshots: {
                        statId: a.id + "_matchlost",
                        label: "191360",
                        value: 0
                    },
                    bulletsFired: {
                        statId: a.id + "_matchwlratio",
                        label: "194826",
                        value: 0,
                        type: "ratio"
                    },
                    hits: {
                        statId: a.id + "_matchplayed",
                        label: "191358",
                        value: 0
                    },
                    accuracy: {
                        statId: a.id + "_bestscore",
                        label: "199912",
                        value: 0
                    }
                };
                d.push(b)
            }),
                {
                    statisticsIds: h.extractStatisticsIds(d),
                    schema: d,
                    processResult: b
                }
        }
        var r = {
            multiplayer: {},
            terrohunt: {}
        };
        return {
            init: function () {
                r.multiplayer.main = l("multiplayer"),
                    r.terrohunt.main = l("terrohunt"),
                    r.multiplayer.secondary = m("multiplayer"),
                    r.terrohunt.secondary = m("terrohunt"),
                    r.multiplayer.mostUsedOperators = n("multiplayer"),
                    r.terrohunt.mostUsedOperators = n("terrohunt"),
                    r.multiplayer.operators = o("multiplayer"),
                    r.terrohunt.operators = o("terrohunt"),
                    r.multiplayer.weapons = p("multiplayer"),
                    r.terrohunt.weapons = p("terrohunt"),
                    r.multiplayer.modes = q("multiplayer"),
                    r.terrohunt.modes = q("terrohunt")
            },
            getEmptyStatisticsSections: function (a, b, c) {
                var d = {};
                return _.forEach(b, function (b) {
                    var c = r[a][b];
                    d[b] = c.schema
                }),
                    e.when(d)
            },
            getStatisticsSections: function (a, c, d) {
                var e, f = {};
                _.forEach(c, function (c) {
                    e = r[a][c],
                        f = b.merge(f, e.statisticsIds)
                });
                for (var h = function (a) {
                    return i.getStatistics({
                        populations: d,
                        statistics: a.join(",")
                    })
                }, j = [], k = _.toArray(f), l = 0; l < k.length;) {
                    var m = _.slice(k, l, l + 15);
                    j.push(h(m)),
                        l += m.length
                }
                return g.qSequence(j, 250).then(function (b) {
                    var f = {};
                    _.forEach(b, function (a) {
                        f = _.merge(f, a)
                    });
                    var h = {};
                    return _.forEach(c, function (b) {
                        e = r[a][b],
                            h[b] = e.processResult(f.results[d])
                    }),
                        g.qSequence(h, 250)
                })
            }
        }
    }
    d.$inject = ["PLAYERSTATISTICS", "$log", "$http", "$q", "rsUtilModelService", "dataUtilsService", "statisticsService", "playerStatApiService", "r6KarmaApiService", "r6PlayerProfileApiService"],
        b.module("g2w.section.playerstatistics").service("playerstatisticsService", d)
}(window, window.angular);

/** module("g2w.section.rewards").config(h) */
!function (a, b, c, d) {
    "use strict";
    function e(a, d, e, f) {
        for (var g, h = parseInt(d.seasonId), i = 0; i < e.seasons.length; i++) {
            var j = e.seasons[i]
                , k = j.header.seasonInformation;
            if (parseInt(k.id) === parseInt(h)) {
                g = c.cloneDeep(j);
                break
            }
        }
        return a(function (a, c) {
            b.isDefined(g) ? a(h) : c(f.generateError("notFound", "rewards.config", "seasonId"))
        })
    }
    function f(a, b, c, d, e) {
        var f;
        return f
    }
    function g(a, d, e, f, g, h) {
        var i;
        return g.getUsersStatistics(d.profiles, h).then(function (a) {
            var d = c.find(a, {
                platformType: e.type
            });
            return b.isDefined(d) && b.isDefined(d.ranks) && (i = d.ranks),
                i
        })
    }
    function h(a, b, c) {
        b.when("/:locale/:platform/rewards", "/:locale/:platform/rewards/season/" + c.season),
            b.when("/:locale/:platform/rewards/season/", "/:locale/:platform/rewards/season/" + c.season),
            b.when("/:locale/rewards", "/:locale/rewards/season/" + c.season),
            b.when("/:locale/rewards/season", "/:locale/rewards/season/" + c.season),
            a.state("rewardsLogged", {
                parent: "platform",
                url: "/rewards/season/:seasonId",
                views: {
                    "section@root": {
                        templateUrl: "assets/tpl/rewards.tpl.html",
                        controller: "rewardsController",
                        controllerAs: "vm"
                    }
                },
                resolve: {
                    seasonId: e,
                    currentRank: g
                },
                data: {
                    permissions: {
                        only: ["player"],
                        redirection: {
                            anonymous: {
                                name: "rewards"
                            }
                        }
                    },
                    tagCommander: {
                        pageName: "Rewards",
                        paramId: "seasonId"
                    }
                }
            }).state("rewards", {
                parent: "locale",
                url: "/rewards/season/:seasonId",
                views: {
                    "section@root": {
                        templateUrl: "assets/tpl/rewards.tpl.html",
                        controller: "rewardsController",
                        controllerAs: "vm"
                    }
                },
                resolve: {
                    seasonId: e,
                    currentRank: f
                },
                data: {
                    permissions: {
                        only: ["anonymous"],
                        redirection: {
                            player: {
                                name: "rewardsLogged"
                            }
                        }
                    },
                    tagCommander: {
                        pageName: "Rewards",
                        paramId: "seasonId"
                    }
                }
            })
    }
    e.$inject = ["$q", "$stateParams", "REWARDS", "errorsService"],
        g.$inject = ["$state", "session", "platform", "rdvServicesConfig", "statisticsService", "seasonId"],
        f.$inject = ["$state", "session", "locale", "statisticsService", "seasonId"],
        h.$inject = ["$stateProvider", "$urlRouterProvider", "CONFIG"],
        b.module("g2w.section.rewards").config(h)
}(window, window.angular, window._);

/** module("g2w.section.rewards").constant("REWARDS", d) */
!function (a, b, c) {
    "use strict";
    var d = {
        seasons: [{
            header: {
                seasonInformation: {
                    id: 2,
                    title: "Dust line",
                    logo: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02 - shield.aeb5d009.png"
                }
            },
            description: {
                nbRankedMatches: 10,
                rewardsImage: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02 - rewards banner.4969ce84.png"
            },
            rankList: [{
                id: "copper-league",
                label: "207563",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02 - copper charm.44c1ede2.png",
                    name: "207563",
                    description: "207564"
                },
                ranks: [{
                    icon: 1,
                    range: {
                        min: 0,
                        max: 2200
                    }
                }, {
                    icon: 2,
                    range: {
                        min: 2200,
                        max: 2400
                    }
                }, {
                    icon: 3,
                    range: {
                        min: 2400,
                        max: 2550
                    }
                }, {
                    icon: 4,
                    range: {
                        min: 2550,
                        max: 2700
                    }
                }]
            }, {
                id: "bronze-league",
                label: "207571",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02 - bronze charm.5edcf1c6.png",
                    name: "207571",
                    description: "207565"
                },
                ranks: [{
                    icon: 5,
                    range: {
                        min: 2700,
                        max: 2800
                    }
                }, {
                    icon: 6,
                    range: {
                        min: 2800,
                        max: 2900
                    }
                }, {
                    icon: 7,
                    range: {
                        min: 2900,
                        max: 3050
                    }
                }, {
                    icon: 8,
                    range: {
                        min: 3050,
                        max: 3200
                    }
                }]
            }, {
                id: "silver-league",
                label: "207570",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02 - silver charm.adde1d01.png",
                    name: "207570",
                    description: "207566"
                },
                ranks: [{
                    icon: 9,
                    range: {
                        min: 3200,
                        max: 3350
                    }
                }, {
                    icon: 10,
                    range: {
                        min: 3350,
                        max: 3520
                    }
                }, {
                    icon: 11,
                    range: {
                        min: 3520,
                        max: 3700
                    }
                }, {
                    icon: 12,
                    range: {
                        min: 3700,
                        max: 3930
                    }
                }]
            }, {
                id: "gold-league",
                label: "207574",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02 - gold charm.1667669d.png",
                    name: "207574",
                    description: "207567"
                },
                ranks: [{
                    icon: 13,
                    range: {
                        min: 3930,
                        max: 4160
                    }
                }, {
                    icon: 14,
                    range: {
                        min: 4160,
                        max: 4400
                    }
                }, {
                    icon: 15,
                    range: {
                        min: 4400,
                        max: 4640
                    }
                }, {
                    icon: 16,
                    range: {
                        min: 4640,
                        max: 4900
                    }
                }]
            }, {
                id: "platinum-league",
                label: "207572",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02 - platinum charm.d7f950d5.png",
                    name: "207572",
                    description: "207568"
                },
                ranks: [{
                    icon: 17,
                    range: {
                        min: 4900,
                        max: 5160
                    }
                }, {
                    icon: 18,
                    range: {
                        min: 5160,
                        max: 5450
                    }
                }, {
                    icon: 19,
                    range: {
                        min: 5450,
                        max: 6e3
                    }
                }]
            }, {
                id: "diamond-league",
                label: "207573",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season02 - diamond charm.e66cad88.png",
                    name: "207573",
                    description: "207569"
                },
                ranks: [{
                    icon: 20,
                    range: {
                        min: 6e3,
                        max: ""
                    }
                }]
            }]
        }, {
            header: {
                seasonInformation: {
                    id: 3,
                    title: "Skull Rain",
                    logo: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season03 - shield.4954f9ed.png"
                }
            },
            description: {
                nbRankedMatches: 10,
                rewardsImage: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season03 - rewards banner.317fabc2.png"
            },
            rankList: [{
                id: "copper-league",
                label: "207563",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season03 - copper charm.c02f56aa.png",
                    name: "207563",
                    description: "207564"
                },
                ranks: [{
                    icon: 1,
                    range: {
                        min: 0,
                        max: 2200
                    }
                }, {
                    icon: 2,
                    range: {
                        min: 2200,
                        max: 2400
                    }
                }, {
                    icon: 3,
                    range: {
                        min: 2400,
                        max: 2550
                    }
                }, {
                    icon: 4,
                    range: {
                        min: 2550,
                        max: 2700
                    }
                }]
            }, {
                id: "bronze-league",
                label: "207571",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season03 - bronze charm.775cec8c.png",
                    name: "207571",
                    description: "207565"
                },
                ranks: [{
                    icon: 5,
                    range: {
                        min: 2700,
                        max: 2800
                    }
                }, {
                    icon: 6,
                    range: {
                        min: 2800,
                        max: 2900
                    }
                }, {
                    icon: 7,
                    range: {
                        min: 2900,
                        max: 3050
                    }
                }, {
                    icon: 8,
                    range: {
                        min: 3050,
                        max: 3200
                    }
                }]
            }, {
                id: "silver-league",
                label: "207570",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season03 - silver charm.88e6115e.png",
                    name: "207570",
                    description: "207566"
                },
                ranks: [{
                    icon: 9,
                    range: {
                        min: 3200,
                        max: 3350
                    }
                }, {
                    icon: 10,
                    range: {
                        min: 3350,
                        max: 3520
                    }
                }, {
                    icon: 11,
                    range: {
                        min: 3520,
                        max: 3700
                    }
                }, {
                    icon: 12,
                    range: {
                        min: 3700,
                        max: 3930
                    }
                }]
            }, {
                id: "gold-league",
                label: "207574",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season03 - gold charm.70d74bdf.png",
                    name: "207574",
                    description: "207567"
                },
                ranks: [{
                    icon: 13,
                    range: {
                        min: 3930,
                        max: 4160
                    }
                }, {
                    icon: 14,
                    range: {
                        min: 4160,
                        max: 4400
                    }
                }, {
                    icon: 15,
                    range: {
                        min: 4400,
                        max: 4640
                    }
                }, {
                    icon: 16,
                    range: {
                        min: 4640,
                        max: 4900
                    }
                }]
            }, {
                id: "platinum-league",
                label: "207572",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season03 - platinum charm.bf00da82.png",
                    name: "207572",
                    description: "207568"
                },
                ranks: [{
                    icon: 17,
                    range: {
                        min: 4900,
                        max: 5160
                    }
                }, {
                    icon: 18,
                    range: {
                        min: 5160,
                        max: 5450
                    }
                }, {
                    icon: 19,
                    range: {
                        min: 5450,
                        max: 6e3
                    }
                }]
            }, {
                id: "diamond-league",
                label: "207573",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season03 - diamond charm.a946066e.png",
                    name: "207573",
                    description: "207569"
                },
                ranks: [{
                    icon: 20,
                    range: {
                        min: 6e3,
                        max: ""
                    }
                }]
            }]
        }, {
            header: {
                seasonInformation: {
                    id: 4,
                    title: "Red Crow",
                    logo: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season04 - shield.ce89e393.png"
                }
            },
            description: {
                nbRankedMatches: 10,
                rewardsImage: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season04 - rewards banner.bc41c03a.png"
            },
            rankList: [{
                id: "copper-league",
                label: "207563",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season04 - copper charm.b601e52f.png",
                    name: "207563",
                    description: "207564"
                },
                ranks: [{
                    icon: 1,
                    range: {
                        min: 0,
                        max: 2200
                    }
                }, {
                    icon: 2,
                    range: {
                        min: 2200,
                        max: 2400
                    }
                }, {
                    icon: 3,
                    range: {
                        min: 2400,
                        max: 2550
                    }
                }, {
                    icon: 4,
                    range: {
                        min: 2550,
                        max: 2700
                    }
                }]
            }, {
                id: "bronze-league",
                label: "207571",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season04 - bronze charm.5b020349.png",
                    name: "207571",
                    description: "207565"
                },
                ranks: [{
                    icon: 5,
                    range: {
                        min: 2700,
                        max: 2800
                    }
                }, {
                    icon: 6,
                    range: {
                        min: 2800,
                        max: 2900
                    }
                }, {
                    icon: 7,
                    range: {
                        min: 2900,
                        max: 3050
                    }
                }, {
                    icon: 8,
                    range: {
                        min: 3050,
                        max: 3200
                    }
                }]
            }, {
                id: "silver-league",
                label: "207570",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season04 - silver charm.4e1149b4.png",
                    name: "207570",
                    description: "207566"
                },
                ranks: [{
                    icon: 9,
                    range: {
                        min: 3200,
                        max: 3350
                    }
                }, {
                    icon: 10,
                    range: {
                        min: 3350,
                        max: 3520
                    }
                }, {
                    icon: 11,
                    range: {
                        min: 3520,
                        max: 3700
                    }
                }, {
                    icon: 12,
                    range: {
                        min: 3700,
                        max: 3930
                    }
                }]
            }, {
                id: "gold-league",
                label: "207574",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season04 - gold charm.2bb0f28e.png",
                    name: "207574",
                    description: "207567"
                },
                ranks: [{
                    icon: 13,
                    range: {
                        min: 3930,
                        max: 4160
                    }
                }, {
                    icon: 14,
                    range: {
                        min: 4160,
                        max: 4400
                    }
                }, {
                    icon: 15,
                    range: {
                        min: 4400,
                        max: 4640
                    }
                }, {
                    icon: 16,
                    range: {
                        min: 4640,
                        max: 4900
                    }
                }]
            }, {
                id: "platinum-league",
                label: "207572",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season04 - platinum charm.a1dd206e.png",
                    name: "207572",
                    description: "207568"
                },
                ranks: [{
                    icon: 17,
                    range: {
                        min: 4900,
                        max: 5160
                    }
                }, {
                    icon: 18,
                    range: {
                        min: 5160,
                        max: 5450
                    }
                }, {
                    icon: 19,
                    range: {
                        min: 5450,
                        max: 6e3
                    }
                }]
            }, {
                id: "diamond-league",
                label: "207573",
                reward: {
                    image: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/season04 - diamond charm.c9cdfcf0.png",
                    name: "207573",
                    description: "207569"
                },
                ranks: [{
                    icon: 20,
                    range: {
                        min: 6e3,
                        max: ""
                    }
                }]
            }]
        }]
    };
    b.module("g2w.section.rewards").constant("REWARDS", d)
}(window, window.angular);

/** module("g2w.section.rewards").controller("rewardsController", e) */
!function (a, b, c, d) {
    "use strict";
    function e(a, d, e, f) {
        for (var g, h, i, j, k, l = this, m = 0; m < a.seasons.length; m++) {
            var n = a.seasons[m]
                , o = n.header.seasonInformation;
            if (parseInt(o.id) === parseInt(d)) {
                if (g = c.cloneDeep(n),
                    h = g.seasonInformation,
                    b.isDefined(e) && !c.isEmpty(e))
                    for (var p = 0; p < g.rankList.length; p++) {
                        for (var q = g.rankList[p], r = 0; r < q.ranks.length; r++) {
                            var s = q.ranks[r];
                            if (parseInt(s.icon) === parseInt(e.current.icon) && (i = q,
                                k = s,
                                j = q.reward),
                                b.isDefined(i) && b.isDefined(k) && b.isDefined(j))
                                break
                        }
                        if (b.isDefined(k) && b.isDefined(j))
                            break
                    }
                break
            }
        }
        var t = "rewards";
        f && (t = "rewardsLogged"),
            l.header = g.header,
            l.header.currentReward = j,
            l.description = g.description,
            l.rankList = g.rankList,
            l.currentRank = e,
            l.navigation = [{
                seasonId: 4,
                uiSref: t + '({seasonId:"4"})',
                label: "207557"
            }, {
                seasonId: 3,
                uiSref: t + '({seasonId:"3"})',
                label: "207557"
            }, {
                seasonId: 2,
                uiSref: t + '({seasonId:"2"})',
                label: "207557"
            }],
            parseInt(o.id) <= 4 && (l.isLegacy = !0)
    }
    e.$inject = ["REWARDS", "seasonId", "currentRank", "session"],
        b.module("g2w.section.rewards").controller("rewardsController", e)
}(window, window.angular, window._);

/** module("config", []).constant("CONFIG") */
/**module("config", []).constant("VERSION")" */
!function (a, b, c) {
    b.module("config", []).constant("CONFIG", {
        season: 5,
        rewardsSeason: 4,
        logLevel: "none",
        platforms: ["xbl", "psn", "uplay"],
        env: "prod",
        appIds: {
            g2w: "39baebad-39e5-4552-8c25-2c9b919064e2",
            uplay: "e3d5ea9e-50bd-43b7-88bf-39794f4e3d40",
            psn: "fb4cc4c9-2063-461d-a1e8-84a7d36525fc",
            xbl: "4008612d-3baf-49e4-957a-33066726a7bc"
        },
        spaceIds: {
            g2w: "632df4a5-4fd7-4ee5-a0c8-1f221f7e585f",
            uplay: "5172a557-50b5-4665-b7db-e3f2e8c5041d",
            psn: "05bfb3f7-6c21-4c42-be1f-97a33fb5cf66",
            xbl: "98a601e5-ca91-4440-b1c5-753f601a2c90"
        },
        sandboxes: {
            uplay: "OSBOR_PC_LNCH_A",
            psn: "OSBOR_PS4_LNCH_A",
            xbl: "OSBOR_XBOXONE_LNCH_A"
        },
        uplayConnect: {
            genomeId: "4f9d4adf-3646-4058-8553-c7b48df556e0",
            staticUrl: "http://static8.ubi.com/private/UAT/u/Uplay",
            iframe: "https://uplayconnect.ubi.com",
            avatarBaseUrl: "https://ubisoft-avatars.akamaized.net"
        },
        ubiservices: {
            baseUrl: "https://public-ubiservices.ubi.com",
            wssBaseUrl: "wss://public-ws.aws-ubiservices.ubi.com"
        },
        market: {
            tagCommander: {
                env: "PROD",
                adProfileUrl: "https://rainbow6.ubi.com/r6-g2w/AdTech/GetAdProfile"
            }
        }
    }).constant("VERSION", "2.0.57")
}(window, window.angular);
