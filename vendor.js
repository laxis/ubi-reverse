/** [rs.util.models] ModelConstant */
!function(a, b, c) {
    "use strict";
    b.module("rs.util.models").constant("MODEL", {
        ctus: {
            sas: {
                id: "sas",
                index: "1",
                operators: ["recruit-sas", "smoke-sas", "mute-sas", "sledge-sas", "thatcher-sas"]
            },
            "fbi-swat": {
                id: "fbi-swat",
                index: "2",
                operators: ["recruit-fbi-swat", "castle-fbi-swat", "ash-fbi-swat", "pulse-fbi-swat", "thermite-fbi-swat"]
            },
            gign: {
                id: "gign",
                index: "3",
                operators: ["recruit-gign", "doc-gign", "rook-gign", "twitch-gign", "montagne-gign"]
            },
            spetsnaz: {
                id: "spetsnaz",
                index: "4",
                operators: ["recruit-spetsnaz", "glaz-spetsnaz", "fuze-spetsnaz", "kapkan-spetsnaz", "tachanka-spetsnaz"]
            },
            "gsg-9": {
                id: "gsg-9",
                index: "5",
                operators: ["recruit-gsg-9", "blitz-gsg-9", "iq-gsg-9", "jager-gsg-9", "bandit-gsg-9"]
            },
            jtf2: {
                id: "jtf2",
                index: "6",
                operators: ["recruit-jtf2", "buck-jtf2", "frost-jtf2"]
            },
            "navy-seal": {
                id: "navy-seal",
                index: "7",
                operators: ["recruit-navy-seal", "blackbeard-navy-seal", "valkyrie-navy-seal"]
            },
            bope: {
                id: "bope",
                index: "8",
                operators: ["recruit-bope", "capitao-bope", "caveira-bope"]
            },
            sat: {
                id: "sat",
                index: "9",
                operators: ["recruit-sat", "hibana-sat", "echo-sat"]
            },
            geo: {
                id: "geo",
                index: "A",
                operators: ["recruit-geo", "jackal-geo", "mira-geo"]
            }
        },
        operators: {
            "smoke-sas": {
                id: "smoke-sas",
                category: "def",
                index: "2",
                stats: ["operatorpvp_smoke_poisongaskill", "operatorpve_smoke_poisongaskill"],
                ctuIndex: "1",
                fullIndex: "2:1",
                heroStats: ["operatorpvp_smoke_poisongaskill", "operatorpve_smoke_poisongaskill"]
            },
            "mute-sas": {
                id: "mute-sas",
                category: "def",
                index: "3",
                stats: ["operatorpvp_mute_gadgetjammed", "operatorpvp_mute_jammerdeployed", "operatorpve_mute_gadgetjammed", "operatorpve_mute_jammerdeployed"],
                ctuIndex: "1",
                fullIndex: "3:1",
                heroStats: ["operatorpvp_mute_gadgetjammed", "operatorpve_mute_gadgetjammed"]
            },
            "sledge-sas": {
                id: "sledge-sas",
                category: "atk",
                index: "4",
                stats: ["operatorpvp_sledge_hammerhole", "operatorpvp_sledge_hammerkill", "operatorpve_sledge_hammerhole", "operatorpve_sledge_hammerkill"],
                ctuIndex: "1",
                fullIndex: "4:1",
                heroStats: ["operatorpvp_sledge_hammerhole", "operatorpve_sledge_hammerhole"]
            },
            "thatcher-sas": {
                id: "thatcher-sas",
                category: "atk",
                index: "5",
                stats: ["operatorpvp_thatcher_gadgetdestroywithemp", "operatorpve_thatcher_gadgetdestroywithemp"],
                ctuIndex: "1",
                fullIndex: "5:1",
                heroStats: ["operatorpvp_thatcher_gadgetdestroywithemp", "operatorpve_thatcher_gadgetdestroywithemp"]
            },
            "castle-fbi-swat": {
                id: "castle-fbi-swat",
                category: "def",
                index: "2",
                stats: ["operatorpvp_castle_kevlarbarricadedeployed", "operatorpve_castle_kevlarbarricadedeployed"],
                ctuIndex: "2",
                fullIndex: "2:2",
                heroStats: ["operatorpvp_castle_kevlarbarricadedeployed", "operatorpve_castle_kevlarbarricadedeployed"]
            },
            "ash-fbi-swat": {
                id: "ash-fbi-swat",
                category: "atk",
                index: "3",
                stats: ["operatorpvp_ash_bonfirekill", "operatorpvp_ash_bonfirewallbreached", "operatorpve_ash_bonfirekill", "operatorpve_ash_bonfirewallbreached"],
                ctuIndex: "2",
                fullIndex: "3:2",
                heroStats: ["operatorpvp_ash_bonfirewallbreached", "operatorpve_ash_bonfirewallbreached"]
            },
            "pulse-fbi-swat": {
                id: "pulse-fbi-swat",
                category: "def",
                index: "4",
                stats: ["operatorpvp_pulse_heartbeatassist", "operatorpvp_pulse_heartbeatspot", "operatorpve_pulse_heartbeatassist", "operatorpve_pulse_heartbeatspot"],
                ctuIndex: "2",
                fullIndex: "4:2",
                heroStats: ["operatorpvp_pulse_heartbeatspot", "operatorpve_pulse_heartbeatspot"]
            },
            "thermite-fbi-swat": {
                id: "thermite-fbi-swat",
                category: "atk",
                index: "5",
                stats: ["operatorpvp_thermite_chargedeployed", "operatorpvp_thermite_chargekill", "operatorpvp_thermite_reinforcementbreached", "operatorpve_thermite_chargedeployed", "operatorpve_thermite_chargekill", "operatorpve_thermite_reinforcementbreached"],
                ctuIndex: "2",
                fullIndex: "5:2",
                heroStats: ["operatorpvp_thermite_reinforcementbreached", "operatorpve_thermite_reinforcementbreached"]
            },
            "doc-gign": {
                id: "doc-gign",
                category: "def",
                index: "2",
                stats: ["operatorpvp_doc_hostagerevive", "operatorpvp_doc_selfrevive", "operatorpvp_doc_teammaterevive", "operatorpve_doc_hostagerevive", "operatorpve_doc_selfrevive", "operatorpve_doc_teammaterevive"],
                ctuIndex: "3",
                fullIndex: "2:3",
                heroStats: ["operatorpvp_doc_teammaterevive", "operatorpve_doc_teammaterevive"]
            },
            "rook-gign": {
                id: "rook-gign",
                category: "def",
                index: "3",
                stats: ["operatorpvp_rook_armorboxdeployed", "operatorpvp_rook_armortakenourself", "operatorpvp_rook_armortakenteammate", "operatorpve_rook_armorboxdeployed", "operatorpve_rook_armortakenourself", "operatorpve_rook_armortakenteammate"],
                ctuIndex: "3",
                fullIndex: "3:3",
                heroStats: ["operatorpvp_rook_armortakenteammate", "operatorpve_rook_armortakenteammate"]
            },
            "twitch-gign": {
                id: "twitch-gign",
                category: "atk",
                index: "4",
                stats: ["operatorpvp_twitch_gadgetdestroybyshockdrone", "operatorpvp_twitch_shockdronekill", "operatorpve_twitch_gadgetdestroybyshockdrone", "operatorpve_twitch_shockdronekill"],
                ctuIndex: "3",
                fullIndex: "4:3",
                heroStats: ["operatorpve_twitch_gadgetdestroybyshockdrone", "operatorpvp_twitch_gadgetdestroybyshockdrone"]
            },
            "montagne-gign": {
                id: "montagne-gign",
                category: "atk",
                index: "5",
                stats: ["operatorpvp_montagne_shieldblockdamage", "operatorpve_montagne_shieldblockdamage"],
                ctuIndex: "3",
                fullIndex: "5:3",
                heroStats: ["operatorpvp_montagne_shieldblockdamage", "operatorpve_montagne_shieldblockdamage"]
            },
            "glaz-spetsnaz": {
                id: "glaz-spetsnaz",
                category: "atk",
                index: "2",
                stats: ["operatorpvp_glaz_sniperkill", "operatorpvp_glaz_sniperpenetrationkill", "operatorpve_glaz_sniperkill", "operatorpve_glaz_sniperpenetrationkill"],
                ctuIndex: "4",
                fullIndex: "2:4",
                heroStats: ["operatorpvp_glaz_sniperkill", "operatorpve_glaz_sniperkill"]
            },
            "fuze-spetsnaz": {
                id: "fuze-spetsnaz",
                category: "atk",
                index: "3",
                stats: ["operatorpvp_fuze_clusterchargekill", "operatorpve_fuze_clusterchargekill"],
                ctuIndex: "4",
                fullIndex: "3:4",
                heroStats: ["operatorpvp_fuze_clusterchargekill", "operatorpve_fuze_clusterchargekill"]
            },
            "kapkan-spetsnaz": {
                id: "kapkan-spetsnaz",
                category: "def",
                index: "4",
                stats: ["operatorpvp_kapkan_boobytrapdeployed", "operatorpvp_kapkan_boobytrapkill", "operatorpve_kapkan_boobytrapdeployed", "operatorpve_kapkan_boobytrapkill"],
                ctuIndex: "4",
                fullIndex: "4:4",
                heroStats: ["operatorpvp_kapkan_boobytrapkill", "operatorpve_kapkan_boobytrapkill"]
            },
            "tachanka-spetsnaz": {
                id: "tachanka-spetsnaz",
                category: "def",
                index: "5",
                stats: ["operatorpvp_tachanka_turretdeployed", "operatorpvp_tachanka_turretkill", "operatorpve_tachanka_turretdeployed", "operatorpve_tachanka_turretkill"],
                ctuIndex: "4",
                fullIndex: "5:4",
                heroStats: ["operatorpvp_tachanka_turretkill", "operatorpve_tachanka_turretkill"]
            },
            "blitz-gsg-9": {
                id: "blitz-gsg-9",
                category: "atk",
                index: "2",
                stats: ["operatorpvp_blitz_flashedenemy", "operatorpvp_blitz_flashfollowupkills", "operatorpvp_blitz_flashshieldassist", "operatorpve_blitz_flashedenemy", "operatorpve_blitz_flashfollowupkills", "operatorpve_blitz_flashshieldassist"],
                ctuIndex: "5",
                fullIndex: "2:5",
                heroStats: ["operatorpvp_blitz_flashedenemy", "operatorpve_blitz_flashedenemy"]
            },
            "iq-gsg-9": {
                id: "iq-gsg-9",
                category: "atk",
                index: "3",
                stats: ["operatorpvp_iq_gadgetspotbyef", "operatorpve_iq_gadgetspotbyef"],
                ctuIndex: "5",
                fullIndex: "3:5",
                heroStats: ["operatorpvp_iq_gadgetspotbyef", "operatorpve_iq_gadgetspotbyef"]
            },
            "jager-gsg-9": {
                id: "jager-gsg-9",
                category: "def",
                index: "4",
                stats: ["operatorpvp_jager_gadgetdestroybycatcher", "operatorpve_jager_gadgetdestroybycatcher"],
                ctuIndex: "5",
                fullIndex: "4:5",
                heroStats: ["operatorpvp_jager_gadgetdestroybycatcher", "operatorpve_jager_gadgetdestroybycatcher"]
            },
            "bandit-gsg-9": {
                id: "bandit-gsg-9",
                category: "def",
                index: "5",
                stats: ["operatorpvp_bandit_batterykill", "operatorpve_bandit_batterykill"],
                ctuIndex: "5",
                fullIndex: "5:5",
                heroStats: ["operatorpvp_bandit_batterykill", "operatorpve_bandit_batterykill"]
            },
            "buck-jtf2": {
                id: "buck-jtf2",
                category: "atk",
                index: "2",
                stats: ["operatorpvp_buck_kill", "operatorpve_buck_kill"],
                ctuIndex: "6",
                fullIndex: "2:6",
                heroStats: ["operatorpvp_buck_kill", "operatorpve_buck_kill"]
            },
            "frost-jtf2": {
                id: "frost-jtf2",
                category: "def",
                index: "3",
                stats: ["operatorpvp_frost_dbno", "operatorpve_frost_beartrap_kill"],
                ctuIndex: "6",
                fullIndex: "3:6",
                heroStats: ["operatorpvp_frost_dbno", "operatorpve_frost_beartrap_kill"]
            },
            "blackbeard-navy-seal": {
                id: "blackbeard-navy-seal",
                category: "atk",
                index: "2",
                stats: ["operatorpvp_blackbeard_gunshieldblockdamage", "operatorpve_blackbeard_gunshieldblockdamage"],
                ctuIndex: "7",
                fullIndex: "2:7",
                heroStats: ["operatorpvp_blackbeard_gunshieldblockdamage", "operatorpve_blackbeard_gunshieldblockdamage"]
            },
            "valkyrie-navy-seal": {
                id: "valkyrie-navy-seal",
                category: "def",
                index: "3",
                stats: ["operatorpvp_valkyrie_camdeployed", "operatorpve_valkyrie_camdeployed"],
                ctuIndex: "7",
                fullIndex: "3:7",
                heroStats: ["operatorpvp_valkyrie_camdeployed", "operatorpve_valkyrie_camdeployed"]
            },
            "capitao-bope": {
                id: "capitao-bope",
                category: "atk",
                index: "2",
                stats: ["operatorpvp_capitao_lethaldartkills", "operatorpvp_capitao_smokedartslaunched", "operatorpve_capitao_lethaldartkills", "operatorpve_capitao_smokedartslaunched"],
                ctuIndex: "8",
                fullIndex: "2:8",
                heroStats: ["operatorpvp_capitao_lethaldartkills", "operatorpve_capitao_lethaldartkills"]
            },
            "caveira-bope": {
                id: "caveira-bope",
                category: "def",
                index: "3",
                stats: ["operatorpvp_caveira_interrogations", "operatorpve_caveira_aikilledinstealth"],
                ctuIndex: "8",
                fullIndex: "3:8",
                heroStats: ["operatorpvp_caveira_interrogations", "operatorpve_caveira_aikilledinstealth"]
            },
            "hibana-sat": {
                id: "hibana-sat",
                category: "atk",
                index: "2",
                stats: ["operatorpvp_hibana_detonate_projectile", "operatorpve_hibana_detonate_projectile"],
                ctuIndex: "9",
                fullIndex: "2:9",
                heroStats: ["operatorpvp_hibana_detonate_projectile", "operatorpve_hibana_detonate_projectile"]
            },
            "echo-sat": {
                id: "echo-sat",
                category: "def",
                index: "3",
                stats: ["operatorpvp_echo_enemy_sonicburst_affected", "operatorpve_echo_enemy_sonicburst_affected"],
                ctuIndex: "9",
                fullIndex: "3:9",
                heroStats: ["operatorpvp_echo_enemy_sonicburst_affected", "operatorpve_echo_enemy_sonicburst_affected"]
            },
            "jackal-geo": {
                id: "jackal-geo",
                category: "atk",
                index: "2",
                stats: ["operatorpvp_cazador_assist_kill", "operatorpve_cazador_assist_kill"],
                ctuIndex: "A",
                fullIndex: "2:A",
                heroStats: ["operatorpvp_cazador_assist_kill", "operatorpve_cazador_assist_kill"]
            },
            "mira-geo": {
                id: "mira-geo",
                category: "def",
                index: "3",
                stats: ["operatorpvp_black_mirror_gadget_deployed", "operatorpve_black_mirror_gadget_deployed"],
                ctuIndex: "A",
                fullIndex: "3:A",
                heroStats: ["operatorpvp_black_mirror_gadget_deployed", "operatorpve_black_mirror_gadget_deployed"]
            }
        },
        skillranks: {
            0: {
                oasisId: "205409"
            },
            1: {
                oasisId: "193671"
            },
            2: {
                oasisId: "193672"
            },
            3: {
                oasisId: "193673"
            },
            4: {
                oasisId: "193674"
            },
            5: {
                oasisId: "193676"
            },
            6: {
                oasisId: "193677"
            },
            7: {
                oasisId: "193678"
            },
            8: {
                oasisId: "193679"
            },
            9: {
                oasisId: "193681"
            },
            10: {
                oasisId: "193682"
            },
            11: {
                oasisId: "193683"
            },
            12: {
                oasisId: "193684"
            },
            13: {
                oasisId: "193686"
            },
            14: {
                oasisId: "193687"
            },
            15: {
                oasisId: "193688"
            },
            16: {
                oasisId: "193689"
            },
            17: {
                oasisId: "193692"
            },
            18: {
                oasisId: "193693"
            },
            19: {
                oasisId: "193694"
            },
            20: {
                oasisId: "193696"
            }
        },
        skillranksLegacy: {
            0: {
                oasisId: "205409"
            },
            1: {
                oasisId: "193674"
            },
            2: {
                oasisId: "193673"
            },
            3: {
                oasisId: "193672"
            },
            4: {
                oasisId: "193671"
            },
            5: {
                oasisId: "193679"
            },
            6: {
                oasisId: "193678"
            },
            7: {
                oasisId: "193677"
            },
            8: {
                oasisId: "193676"
            },
            9: {
                oasisId: "193684"
            },
            10: {
                oasisId: "193683"
            },
            11: {
                oasisId: "193682"
            },
            12: {
                oasisId: "193681"
            },
            13: {
                oasisId: "193689"
            },
            14: {
                oasisId: "193688"
            },
            15: {
                oasisId: "193687"
            },
            16: {
                oasisId: "193686"
            },
            17: {
                oasisId: "193694"
            },
            18: {
                oasisId: "193693"
            },
            19: {
                oasisId: "193692"
            },
            20: {
                oasisId: "193696"
            }
        },
        shootingweapontypes: {
            "assault-rifle": {
                id: "assault-rifle",
                index: "1",
                icon: "wcat-ar"
            },
            smg: {
                id: "smg",
                index: "2",
                icon: "wcat-smg"
            },
            lmg: {
                id: "lmg",
                index: "3",
                icon: "wcat-lmg"
            },
            sniper: {
                id: "sniper",
                index: "4",
                icon: "wcat-sniper"
            },
            pistol: {
                id: "pistol",
                index: "5",
                icon: "wcat-hg"
            },
            shotgun: {
                id: "shotgun",
                index: "6",
                icon: "wcat-sg"
            },
            "machine-pistol": {
                id: "machine-pistol",
                index: "7",
                icon: "wcat-mp"
            },
            shield: {
                id: "shield",
                index: "8"
            },
            launcher: {
                id: "launcher",
                index: "9"
            }
        }
    })
}(window, window.angular);
/** [rs.util.models] rsUtilModelService */
!function(a, b, c, d) {
    "use strict";
    function e(a, e) {
        var f = {
            "jager-gsg-9": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-jaeger.edd9f749.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-jaeger.bc9087b7.png"
            },
            "blitz-gsg-9": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-blitz.2e74d4b8.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-blitz.0553bf11.png"
            },
            "iq-gsg-9": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-iq.22b58f3b.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-iq.0d7885fb.png"
            },
            "bandit-gsg-9": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-bandit.463ee829.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-bandit.113ec809.png"
            },
            "fuze-spetsnaz": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-fuze.5f8fb3ba.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-fuze.279743fa.png"
            },
            "kapkan-spetsnaz": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-kapkan.606a9fc0.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-kapkan.b796e606.png"
            },
            "glaz-spetsnaz": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-glaz.8cd96a16.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-glaz.a89bfa89.png"
            },
            "tachanka-spetsnaz": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-tachanka.41caebce.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-tachanka.4c41f39c.png"
            },
            "thermite-fbi-swat": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-thermite.e973bb04.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-thermite.f9441d1f.png"
            },
            "castle-fbi-swat": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-castle.6a1677f5.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-castle.cd963832.png"
            },
            "ash-fbi-swat": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-ash.9d28aebe.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-ash.692668cc.png"
            },
            "pulse-fbi-swat": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-pulse.30ab3682.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-pulse.927fa707.png"
            },
            "twitch-gign": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-twitch.84ad765c.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-twitch.0421265d.png"
            },
            "montagne-gign": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-montagne.d48591cc.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-montagne.0bc51336.png"
            },
            "rook-gign": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-rook.eed2777a.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-rook.55b6bc4c.png"
            },
            "doc-gign": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-doc.0b0321eb.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-doc.2cc6664a.png"
            },
            "sledge-sas": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-sledge.832f6c6b.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-sledge.9ade04d3.png"
            },
            "thatcher-sas": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-thatcher.73132fcd.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-thatcher.df2b7de4.png"
            },
            "mute-sas": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-mute.ae51429f.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-mute.96a65665.png"
            },
            "smoke-sas": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-smoke.1bf90066.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-smoke.2726e308.png"
            },
            "frost-jtf2": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-frost.f4325d10.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-frost.a7f18f5b.png"
            },
            "buck-jtf2": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-buck.78712d24.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-buck.6b66cd57.png"
            },
            "valkyrie-navy-seal": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-valkyrie.c1f143fb.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-valkyrie.b2df5bae.png"
            },
            "blackbeard-navy-seal": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-blackbeard.2292a791.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-blackbeard.cba9e22d.png"
            },
            "capitao-bope": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-capitao.984e75b7.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-capitao.31c21fd0.png"
            },
            "caveira-bope": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-caveira.e4d82365.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-caveira.f7bb7af3.png"
            },
            "hibana-sat": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-hibana.d3ceb775.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-hibana.494345ca.png"
            },
            "echo-sat": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-echo.592cfb34.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-echo.6c60bd2a.png"
            },
            "mira-geo": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-mira.0c9e3bd8.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-mira.4196bc07.png"
            },
            "jackal-geo": {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-jackal.e7ec96e6.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-jackal.8c3e4191.png"
            },
            attacker: {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-no-data-attacker.525831e0.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-no-data-attacker.png"
            },
            defender: {
                large: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/large-no-data-defender.b666ab96.png",
                small: "https://ubistatic-a.akamaihd.net/0058/prod/assets/images/small-no-data-defender.png"
            }
        };
        return {
            operatorsImages: f,
            getModel: function() {
                return e
            },
            getOperatorLabel: function(a) {
                return "model.operators." + a
            },
            getOperatorImages: function(a) {
                return f[a]
            },
            getOperatorRoleImages: function(a) {
                return f[a]
            },
            getOperatorList: function(a, d) {
                var f = [];
                return c.forEach(e.operators, function(c) {
                    var e = a(c);
                    b.isDefined(d) ? d(c) && f.push(e) : f.push(e)
                }),
                f
            },
            getCtuLabel: function(a) {
                return "model.ctus." + a
            },
            getCtuLabelFromOperator: function(a) {
                var b, c = e.operators[a];
                for (var d in e.ctus) {
                    var f = e.ctus[d];
                    if (f.index === c.ctuIndex) {
                        b = f;
                        break
                    }
                }
                return "model.ctus." + b.id
            },
            getWeaponLabel: function(a) {
                return "model.shootingweapontypes." + a
            },
            getWeaponIcon: function(a) {
                return e.shootingweapontypes[a].icon
            },
            getRankLabel: function(a, c) {
                var d, f;
                return f = c ? e.skillranksLegacy[a] : e.skillranks[a],
                b.isDefined(f) && (d = f.oasisId),
                d
            },
            getById: function(a, b) {
                try {
                    return c.cloneDeep(e[a][b])
                } catch (f) {
                    return console.error(f),
                    d
                }
            },
            getByIndex: function(a, b) {
                return c.cloneDeep(c.find(e[a], {
                    index: b
                }))
            },
            getByFields: function(a, b, d) {
                for (var f = {}, g = 0; g < b.length; g++)
                    f[b[g]] = d[g];
                return c.cloneDeep(c.find(e[a], f))
            }
        }
    }
    e.$inject = ["$log", "MODEL"],
    b.module("rs.util.models").service("rsUtilModelService", e)
}(window, window.angular, window._);
