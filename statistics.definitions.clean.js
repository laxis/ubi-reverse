module.exports = {
    pve: {
        terrohunt: {
            coop: {
                normalBestScore: "allterrohuntcoop_normal_bestscore",
                hardBestScore: "allterrohuntcoop_hard_bestscore",
                realisticBestScore: "allterrohuntcoop_realistic_bestscore"
            },
            solo: {
                normalBestScore: "allterrohuntsolo_normal_bestscore",
                hardBestScore: "allterrohuntsolo_hard_bestscore",
                realisticBestScore: "allterrohuntsolo_realistic_bestscore"
            }
        },
        gadget: {
            chosen: "gadgetpve_chosen",
            destroyed: "gadgetpve_gadgetdestroy",
            kills: "gadgetpve_kills",
            mostUsed: "gadgetpve_mostused"
        },
        general: {
            accuracy: "generalpve_accuracy",
            barricades: "generalpve_barricadedeployed",
            blindKills: "generalpve_blindkills",
            bulletsFired: "generalpve_bulletfired",
            bulletsHit: "generalpve_bullethit",
            dbno: "generalpve_dbno",
            dbnoAssists: "generalpve_dbnoassists",
            death: "generalpve_death",
            distanceTravelled: "generalpve_distancetravelled",
            gadgetsDestroyed: "generalpve_gadgetdestroy",
            headshots: "generalpve_headshot",
            hostageDefense: "generalpve_hostagedefense",
            hostageRescue: "generalpve_hostagerescue",
            kdRatio: "generalpve_kdratio",
            killAssists: "generalpve_killassists",
            kills: "generalpve_kills",
            matcheslost: "generalpve_matchlost",
            matchesPlayed: "generalpve_matchplayed",
            matchesWlRatio: "generalpve_matchwlratio",
            matchesWon: "generalpve_matchwon",
            meleeKills: "generalpve_meleekills",
            penetrationKills: "generalpve_penetrationkills",
            rappelBreaches: "generalpve_rappelbreach",
            reinforcementsDeployed: "generalpve_reinforcementdeploy",
            revives: "generalpve_revive",
            revivesDenied: "generalpve_revivedenied",
            serverAggression: "generalpve_serveraggression",
            serverDefender: "generalpve_serverdefender",
            serversHacked: "generalpve_servershacked",
            suicides: "generalpve_suicide",
            timePlayed: "generalpve_timeplayed",
            totalXp: "generalpve_totalxp"
        }
    },
    pvp: {
        casual: {
            deaths: "casualpvp_death",
            kdRatio: "casualpvp_kdratio",
            kills: "casualpvp_kills",
            matchesLost: "casualpvp_matchlost",
            matchesPlayed: "casualpvp_matchplayed",
            matchesWlRatio: "casualpvp_matchwlratio",
            matchesWon: "casualpvp_matchwon",
            timePlayed: "casualpvp_timeplayed"
        },
        custom: {
            timePlayed: "custompvp_timeplayed"
        },
        gadget: {
            chosen: "gadgetpvp_chosen",
            destroyed: "gadgetpvp_gadgetdestroy",
            kills: "gadgetpvp_kills",
            mostUsed: "gadgetpvp_mostused"
        },
        gamemodeOperator: {
            matchesLost: "gamemodeoperatorpvp_matchlost",
            matchesPlayed: "gamemodeoperatorpvp_matchplayed",
            matchesWlRatio: "gamemodeoperatorpvp_matchwlratio",
            matchesWon: "gamemodeoperatorpvp_matchwon"
        },
        karmaRank: "karma_rank",
        general: {
            accuracy: "generalpvp_accuracy",
            barricades: "generalpvp_barricadedeployed",
            blindKills: "generalpvp_blindkills",
            bulletsFired: "generalpvp_bulletfired",
            bulletsHit: "generalpvp_bullethit",
            dbno: "generalpvp_dbno",
            dbnoAssists: "generalpvp_dbnoassists",
            death: "generalpvp_death",
            distanceTravelled: "generalpvp_distancetravelled",
            gadgetsDestroyed: "generalpvp_gadgetdestroy",
            headshots: "generalpvp_headshot",
            hostageDefense: "generalpvp_hostagedefense",
            hostageRescue: "generalpvp_hostagerescue",
            kdRatio: "generalpvp_kdratio",
            killAssists: "generalpvp_killassists",
            kills: "generalpvp_kills",
            matcheslost: "generalpvp_matchlost",
            matchesPlayed: "generalpvp_matchplayed",
            matchesWlRatio: "generalpvp_matchwlratio",
            matchesWon: "generalpvp_matchwon",
            meleeKills: "generalpvp_meleekills",
            penetrationKills: "generalpvp_penetrationkills",
            rappelBreaches: "generalpvp_rappelbreach",
            reinforcementsDeployed: "generalpvp_reinforcementdeploy",
            revives: "generalpvp_revive",
            revivesDenied: "generalpvp_revivedenied",
            serverAggression: "generalpvp_serveraggression",
            serverDefender: "generalpvp_serverdefender",
            serversHacked: "generalpvp_servershacked",
            suicides: "generalpvp_suicide",
            timePlayed: "generalpvp_timeplayed",
            totalXp: "generalpvp_totalxp"
        }
    }
}